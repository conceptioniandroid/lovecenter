package com.marvix.lovetv;

import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.marvix.lovetv.activity.HomeActivity;
import com.marvix.lovetv.api.YoutubeDataClient;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.LifeCycleCallback;
import com.onesignal.OSNotification;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

public class LoveCenter extends Application {

    private static LoveCenter mInstance;
    String type = "";
    String blog_id = "";

    public LoveCenter() {
        mInstance = this;
    }

    public static Context getContext() {
        return mInstance;
    }

    @Override
    protected void attachBaseContext(Context context) {
        super.attachBaseContext(context);

    }

    @Override
    public void onCreate() {
        super.onCreate();
        youtubeClient = new YoutubeDataClient();
        registerActivityLifecycleCallbacks(new LifeCycleCallback());

        // OneSignal Initialization
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationReceivedHandler(new OneSignal.NotificationReceivedHandler() {
                    @Override
                    public void notificationReceived(OSNotification notification) {
                        Log.d("++++notification", "++ " + notification.toString());
                        JSONObject data = notification.payload.additionalData;
                        blog_id = data.optString("post_id");
                        type = data.optString("type");
                        Log.d("++++data", "+++ " + data + "++++" + blog_id + " ++++++type " + type);
                    }
                })
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        if (type.equalsIgnoreCase("post")) {
                            Constant.flag = "fromNotification";
                            Intent intent = new Intent(LoveCenter.this, HomeActivity.class).putExtra("blogId", blog_id);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (type.equalsIgnoreCase("event")) {
                            Constant.flag = "fromNotificationEvent";
                            Intent intent = new Intent(LoveCenter.this, HomeActivity.class).putExtra("blogId", blog_id);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else if (type.equalsIgnoreCase("sermons")) {
                            Log.d("TAG", "notificationOpened:type3 " + type);
                            Constant.flag = "fromNotificationAudio";
                            Intent intent = new Intent(LoveCenter.this, HomeActivity.class).putExtra("blogId", blog_id);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                })
                .init();
    }

    private static YoutubeDataClient youtubeClient;

    public static YoutubeDataClient getYoutubeClient() {
        return youtubeClient;
    }

}
