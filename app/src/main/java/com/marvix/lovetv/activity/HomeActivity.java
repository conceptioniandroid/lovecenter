package com.marvix.lovetv.activity;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.marvix.lovetv.R;
import com.marvix.lovetv.fragment.AudioFragment;
import com.marvix.lovetv.fragment.BlogFragment;
import com.marvix.lovetv.fragment.EventFragment;
import com.marvix.lovetv.fragment.HomeFragment;
import com.marvix.lovetv.fragment.OurChurchFragment;
import com.marvix.lovetv.fragment.SettingFragment;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.SharedPrefs;

public class HomeActivity extends AppCompatActivity {
    //    BottomNavigationViewEx bnve;
    BottomNavigationView bnve;
    int gettabpos = 0;
    FragmentTransaction ft;
    String blogId = "";
    public static MediaPlayer mp;
    public static NotificationCompat.Builder notificationBuilder;
    public static NotificationManager notificationManager;
    public static ImageView btnPlay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        init();
        clicks();
    }

    private void clicks() {

    }

    private void init() {

        bnve = (BottomNavigationView) findViewById(R.id.bnve);
        bnve.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener);
        bnve.setOnNavigationItemReselectedListener(onNavigationItemReselectedListener);

        BottomNavigationMenuView menuView = (BottomNavigationMenuView)
                bnve.getChildAt(0);

        for (int i = 0; i < menuView.getChildCount(); i++) {
            final View iconView =
                    menuView.getChildAt(i).findViewById(android.support.design.R.id.icon);
            final ViewGroup.LayoutParams layoutParams =
                    iconView.getLayoutParams();
            final DisplayMetrics displayMetrics =
                    getResources().getDisplayMetrics();
            layoutParams.height = (int)
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.bottomnavicon),
                            displayMetrics);
            layoutParams.width = (int)
                    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.bottomnavicon),
                            displayMetrics);
            iconView.setLayoutParams(layoutParams);
        }
        if (getIntent().getExtras() != null) {
            Intent intent = getIntent();
            blogId = intent.getStringExtra("blogId");
        }
        if (Constant.flag.equalsIgnoreCase("fromNotification")) {
            Fragment fragment = new BlogFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Bundle bundle = new Bundle();
            bundle.putString("blogId", blogId);
            fragment.setArguments(bundle);
            ft.addToBackStack("");
            ft.replace(R.id.content, fragment);
            ft.commit();
        } else if (Constant.flag.equalsIgnoreCase("fromNotificationEvent")) {
            Fragment fragment = new EventFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            Bundle bundle1 = new Bundle();
            bundle1.putString("eventId", blogId);
            Log.d("++++blogid", "++home " + blogId);
            fragment.setArguments(bundle1);
            ft.addToBackStack("");
            ft.replace(R.id.content, fragment);
            ft.commit();
        } else if (Constant.flag.equalsIgnoreCase("fromNotificationAudio")) {
            Fragment fragment = new AudioFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.content, fragment);
            ft.addToBackStack("");
            ft.commit();
        } else {
            Fragment fragment = new HomeFragment();
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction ft = fragmentManager.beginTransaction();
            ft.replace(R.id.content, fragment);
            ft.commit();
        }
    }

    BottomNavigationView.OnNavigationItemReselectedListener onNavigationItemReselectedListener = new BottomNavigationView.OnNavigationItemReselectedListener() {
        @Override
        public void onNavigationItemReselected(@NonNull MenuItem menuItem) {
            switch (menuItem.getItemId()) {
                case R.id.home:
                    changeFragment(new HomeFragment());
                    break;

                case R.id.church:
                    changeFragment(new OurChurchFragment());
                    ft.addToBackStack("");
                    break;
                case R.id.pray:
                    ShowAlert();
                    break;
                case R.id.share:
                    try {
                        Intent i = new Intent(Intent.ACTION_SEND);
                        i.setType("text/plain");
                        i.putExtra(Intent.EXTRA_SUBJECT, "Love Center");
                        String sAux = "\nLet me recommend you this application\n\n";
//                        sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                        sAux = sAux + "https://felixosborn.org/love-center-app/ \n\n";
                        i.putExtra(Intent.EXTRA_TEXT, sAux);
                        startActivity(Intent.createChooser(i, "choose one"));
                    } catch (Exception e) {
                        //e.toString();
                    }
                    break;
                case R.id.logout:
                    changeFragment(new SettingFragment());
                    break;
            }
        }
    };
    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationItemSelectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
            if (bnve.getSelectedItemId() != menuItem.getItemId()) {
                switch (menuItem.getItemId()) {
                    case R.id.home:
                        Log.d("+++++id", "++ " + bnve.getSelectedItemId() + "+++++" + menuItem.getItemId());
                        changeFragment(new HomeFragment());
                        return true;
                    case R.id.church:
                        changeFragment(new OurChurchFragment());
                        ft.addToBackStack("");
                        return true;
                    case R.id.pray:
                        ShowAlert();
                        return true;
                    case R.id.share:
                        try {
                            Intent i = new Intent(Intent.ACTION_SEND);
                            i.setType("text/plain");
                            i.putExtra(Intent.EXTRA_SUBJECT, "Love Center");
                            String sAux = "\nLet me recommend you this application\n\n";
//                            sAux = sAux + "https://play.google.com/store/movies/details/Jurassic_World_Fallen_Kingdom?id=OexCdfICdGU \n\n";
                            sAux = sAux + "https://felixosborn.org/love-center-app/ \n\n";
                            i.putExtra(Intent.EXTRA_TEXT, sAux);
                            startActivity(Intent.createChooser(i, "choose one"));
                        } catch (Exception e) {
                            //e.toString();
                        }
                        return true;
                    case R.id.logout:
//                        showLogoutAlert();
                        changeFragment(new SettingFragment());
                        return true;
                }
            }
            return false;
        }
    };

    public void changeFragment(Fragment targetFragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, targetFragment);
        ft.commit();
    }

    private void ShowAlert() {
        new AlertDialog.Builder(HomeActivity.this)
                .setTitle("Prayer")
                .setMessage("Please send your prayer request here")
                .setCancelable(true)
                .setPositiveButton("Send Email", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        try {
                            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                                    "mailto", "prayer@felixosborn.org", null));
                            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Love Tv");
                            emailIntent.putExtra(Intent.EXTRA_TEXT, "Sent Mail");
                            startActivity(Intent.createChooser(emailIntent, "Send email..."));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                })
                .create().show();
    }


}
