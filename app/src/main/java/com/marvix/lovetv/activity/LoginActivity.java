package com.marvix.lovetv.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.marvix.lovetv.R;
import com.marvix.lovetv.dialogue.ForgetPassword;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.EditTextExtended;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewLight;
import com.marvix.lovetv.utils.Validations;

public class LoginActivity extends AppCompatActivity {
    EditTextExtended edtpass,edtPhoneNo;
    ProgressBar progress;
    TextviewLight tvlforget;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        ImageView linbg = findViewById(R.id.linbg);
        linbg.getLayoutParams().height = (int) (height * 0.30);
        linbg.requestLayout();

        Log.d("TAG", "onCreate: "+height+" "+width);

        init();
        clicks();
    }

    private void clicks() {
        findViewById(R.id.tvrLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validations validations=new Validations();
                if (!validations.isEmpty(edtPhoneNo)&& !validations.isEmpty(edtpass)){
                    if (validations.isValidPhoneNumber(edtPhoneNo)){
                       callLogin();
                    }else new MakeToast(R.string.phoneValidation);
                }else new MakeToast(R.string.allValidation);
            }
        });
        findViewById(R.id.tvlSignup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoginActivity.this,RegistrationActivity.class));
                finish();
            }
        });
        tvlforget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ForgetPassword(LoginActivity.this){

                }.ShowForgetDialogue();
            }
        });
    }

    private void init() {
        edtpass=findViewById(R.id.edtpass);
        edtPhoneNo=findViewById(R.id.edtPhoneNo);
        progress=findViewById(R.id.progress);
        tvlforget=findViewById(R.id.tvlforget);
    }
    private void callLogin() {
        progress.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(LoginActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.login, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++reg","+++json "+jsonObject);
                        if (jsonObject.optInt("success")==1) {
                            JSONObject object=jsonObject.getJSONObject("data");
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.user_id,  object.optString("user_id")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.var_name,  object.optString("var_name")).apply();
                            progress.setVisibility(View.GONE);
                            startActivity(new Intent(LoginActivity.this,HomeActivity.class));
                            finish();
                        }else {
                            progress.setVisibility(View.GONE);
                            new MakeToast(jsonObject.optString("message"));
                        }
                        progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "login");
                params.put("var_mobileno", edtPhoneNo.getText().toString());
                params.put("var_password", edtpass.getText().toString());
                params.put("device_id", SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken,Constant.notAvailable));

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

}
