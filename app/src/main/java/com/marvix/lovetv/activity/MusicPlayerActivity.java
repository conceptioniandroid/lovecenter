package com.marvix.lovetv.activity;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaPlayer;
import android.media.RingtoneManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RemoteViews;
import android.widget.SeekBar;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.CloseMusicReceiver;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.DownloadService;
import com.marvix.lovetv.utils.MusicReceiver;
import com.marvix.lovetv.utils.TextviewRegular;
import com.marvix.lovetv.utils.Utilities;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;

import dm.audiostreamer.MediaMetaData;

import static com.marvix.lovetv.activity.HomeActivity.btnPlay;
import static com.marvix.lovetv.activity.HomeActivity.notificationBuilder;
import static com.marvix.lovetv.activity.HomeActivity.notificationManager;

public class MusicPlayerActivity extends AppCompatActivity implements MediaPlayer.OnCompletionListener, SeekBar.OnSeekBarChangeListener {

    TextviewRegular title, songCurrentDurationLabel, songTotalDurationLabel;
    ImageView btnNext, btnPrevious;
    private SeekBar songProgressBar;

    private Handler mHandler = new Handler();
    private Utilities utils;
    private int currentSongIndex = 0;
    ArrayList<MediaMetaData> topAudioModelArrayList = new ArrayList<>();
    int position = 0;
    TextviewRegular audiotitle;
    ImageView img;
    ImageView download;
    LinearLayout lin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_music_player);
        Constant.isPlaying = true;

        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;


        Intent intent = getIntent();
        position = Integer.parseInt(intent.getStringExtra("position"));
        currentSongIndex = position;
        Bundle args = intent.getBundleExtra("BUNDLE");
        topAudioModelArrayList.addAll((ArrayList<MediaMetaData>) args.getSerializable("ARRAYLIST"));

        if (topAudioModelArrayList.size() > 0) {
            lin = findViewById(R.id.lin);
            initialize();
            clicks();
            playSong(position);

            download = findViewById(R.id.download);
            if (!isNetConnectionAvailable())
                download.setVisibility(View.GONE);
            download.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startDownload();
                }
            });


            img.getLayoutParams().width = (int) (width * 0.60);
            img.getLayoutParams().height = (int) (width * 0.60);
            img.requestLayout();

            songProgressBar.getLayoutParams().width = (int) (width * 0.70);
            songProgressBar.requestLayout();

            songProgressBar.getLayoutParams().width = (int) (width * 0.70);
            songProgressBar.requestLayout();

            lin.getLayoutParams().width = (int) (width * 0.70);
            lin.requestLayout();

        }
    }

    private boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) LoveCenter.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void startDownload() {
        if (!Constant.isDownload) {
            MediaMetaData media = topAudioModelArrayList.get(currentSongIndex);
            Toast.makeText(this, "Downloading Started...", Toast.LENGTH_SHORT).show();
            Constant.isDownload = true;
            Intent intent = new Intent(this, DownloadService.class);
            intent.putExtra("int_glcode", media.getMediaUrl());
            intent.putExtra("image", media.getMediaArt());
            intent.putExtra("var_link", media.getMediaUrl());
            intent.putExtra("var_title", media.getMediaTitle());
            intent.putExtra("var_description", media.getMediaArtist());
            intent.putExtra("audio_len", media.getMediaLength());
            intent.putExtra("Audio_Length", media.getMediaDuration());
            startService(intent);
        } else {
            Toast.makeText(this, "Download process is alredy running...", Toast.LENGTH_SHORT).show();
        }
    }

    private void clicks() {
        btnPlay.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check for already playing
                RemoteViews notificationView = new RemoteViews(getPackageName(),
                        R.layout.notification_layout);
                if (HomeActivity.mp.isPlaying()) {
                    if (HomeActivity.mp != null) {
                        Constant.isPlaying = false;
                        HomeActivity.mp.pause();
                        // Changing button image to play button
                        btnPlay.setImageResource(R.drawable.play);
                        notificationView.setImageViewResource(R.id.image, R.drawable.logo);
                        notificationView.setImageViewResource(R.id.ivplay, R.drawable.play);
                        notificationManager.notify(0, notificationBuilder.build());
                    }
                } else {
                    // Resume song
                    if (HomeActivity.mp != null) {
                        Constant.isPlaying = true;
                        HomeActivity.mp.start();
                        // Changing button image to pause button
                        btnPlay.setImageResource(R.drawable.puse);
                        notificationView.setImageViewResource(R.id.image, R.drawable.logo);
                        notificationView.setImageViewResource(R.id.ivplay, R.drawable.puse);
                        notificationManager.notify(0, notificationBuilder.build());
                    }
                }

                notificationBuilder.setContent(notificationView);
                notificationManager.notify(0, notificationBuilder.build());

            }
        });

        btnNext.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // check if next song is there or not
                if (currentSongIndex < (topAudioModelArrayList.size() - 1)) {
                    playSong(currentSongIndex + 1);
                    currentSongIndex = currentSongIndex + 1;
                } else {
                    // play first song
                    playSong(0);
                    currentSongIndex = 0;
                }

            }
        });

        btnPrevious.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                if (currentSongIndex > 0) {
                    playSong(currentSongIndex - 1);
                    currentSongIndex = currentSongIndex - 1;
                } else {
                    // play last song
                    playSong(topAudioModelArrayList.size() - 1);
                    currentSongIndex = topAudioModelArrayList.size() - 1;
                }

            }
        });
    }

    public void playSong(int songIndex) {

        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);

        audiotitle.setText(topAudioModelArrayList.get(songIndex).getMediaTitle());
        Glide.with(MusicPlayerActivity.this).load(topAudioModelArrayList.get(songIndex).getMediaArt()).apply(options).into(img);
        try {

            Log.d("MYTAG", "playSong: " + topAudioModelArrayList.get(songIndex).getMediaUrl());

            HomeActivity.mp.reset();
            HomeActivity.mp.setDataSource(topAudioModelArrayList.get(songIndex).getMediaUrl());
            HomeActivity.mp.prepare();
            HomeActivity.mp.start();
            // Displaying Song title
            String songTitle = "Hello";
            // songTitleLabel.setText(songTitle);

            // Changing Button Image to pause image
            btnPlay.setImageResource(R.drawable.puse);

            // set Progress bar values
            songProgressBar.setProgress(0);
            songProgressBar.setMax(100);

            // Updating progress bar
            updateProgressBar();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                mChannel.setName("LoveTv");
                notificationManager.createNotificationChannel(mChannel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        try {
            notificationBuilder = new NotificationCompat.Builder(this, CHANNEL_ID)
                    .setSmallIcon(R.drawable.lv_tv)
                    .setContentTitle(topAudioModelArrayList.get(songIndex).getMediaTitle())
                    .setContentText("")
                    .setAutoCancel(false)
                    .setOngoing(true);

            RemoteViews notificationView = new RemoteViews(getPackageName(),
                    R.layout.notification_layout);
            notificationView.setImageViewResource(R.id.ivplay, R.drawable.puse);
            notificationView.setTextViewText(R.id.tv, topAudioModelArrayList.get(songIndex).getMediaTitle());
            notificationView.setImageViewResource(R.id.image, R.drawable.logo);

            Intent notificationIntent = new Intent(this, MusicReceiver.class);
            notificationIntent.putExtra("type", "play");
            PendingIntent pendingSwitchIntent = PendingIntent.getBroadcast(this, 0,
                    notificationIntent, 0);
            notificationView.setOnClickPendingIntent(R.id.ivplay,
                    pendingSwitchIntent);

            Intent notificationIntent1 = new Intent(this, CloseMusicReceiver.class);
            notificationIntent1.putExtra("type", "close");
            PendingIntent pendingSwitchIntent1 = PendingIntent.getBroadcast(this, 0,
                    notificationIntent1, 0);
            notificationView.setOnClickPendingIntent(R.id.ivclose,
                    pendingSwitchIntent1);

            notificationBuilder.setContent(notificationView);
            notificationBuilder.setContentIntent(pendingSwitchIntent);
            notificationBuilder.setContentIntent(pendingSwitchIntent1);

            notificationManager.notify(0, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
        }
//


    }

    public void updateProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 100);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = HomeActivity.mp.getDuration();
            long currentDuration = HomeActivity.mp.getCurrentPosition();

            // Displaying Total Duration time
            songTotalDurationLabel.setText("" + utils.milliSecondsToTimer(totalDuration));
            // Displaying time completed playing
            songCurrentDurationLabel.setText("" + utils.milliSecondsToTimer(currentDuration));

            // Updating progress bar
            int progress = (int) (utils.getProgressPercentage(currentDuration, totalDuration));
            //Log.d("Progress", ""+progress);
            songProgressBar.setProgress(progress);

            // Running this thread after 100 milliseconds
            mHandler.postDelayed(this, 100);
        }
    };

    private void initialize() {
        songCurrentDurationLabel = findViewById(R.id.songCurrentDurationLabel);
        songTotalDurationLabel = findViewById(R.id.songTotalDurationLabel);
        btnPlay = findViewById(R.id.btnPlay);
        btnNext = findViewById(R.id.btnNext);
        btnPrevious = findViewById(R.id.btnPrevious);
        songProgressBar = findViewById(R.id.songProgressBar);
        audiotitle = findViewById(R.id.audiotitle);
        img = findViewById(R.id.img);

        HomeActivity.mp = new MediaPlayer();

        utils = new Utilities();
        songProgressBar.setOnSeekBarChangeListener(this); // Important
        HomeActivity.mp.setOnCompletionListener(this);
    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        if (currentSongIndex < (topAudioModelArrayList.size() - 1)) {
            playSong(currentSongIndex + 1);
            currentSongIndex = currentSongIndex + 1;
        } else {
            // play first song
            playSong(0);
            currentSongIndex = 0;
        }
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mHandler.removeCallbacks(mUpdateTimeTask);
        int totalDuration = HomeActivity.mp.getDuration();
        int currentPosition = utils.progressToTimer(seekBar.getProgress(), totalDuration);

        // forward or backward to certain seconds
        HomeActivity.mp.seekTo(currentPosition);

        // update timer progress again
        updateProgressBar();
    }

    @Override
    protected void onPause() {
        mHandler.removeCallbacks(mUpdateTimeTask);
        Constant.isScreen = false;
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        Constant.isScreen = true;
    }
}
