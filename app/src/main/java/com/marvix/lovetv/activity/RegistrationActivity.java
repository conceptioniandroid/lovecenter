package com.marvix.lovetv.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.EditTextExtended;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewRegular;
import com.marvix.lovetv.utils.Validations;

public class RegistrationActivity extends AppCompatActivity {
    TextviewRegular tvrSignup;
    EditTextExtended edtName,edtEmail,edtPhone,edtPassword,edtRePassword;
    ProgressBar progress;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
        init();
        clicks();

    }

    private void clicks() {
        findViewById(R.id.tvlLogin).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistrationActivity.this,LoginActivity.class));
                finish();
            }
        });
        tvrSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Validations validations=new Validations();
                if (!validations.isEmpty(edtName) && !validations.isEmpty(edtEmail) && !validations.isEmpty(edtPhone) && !validations.isEmpty(edtPhone) && !validations.isEmpty(edtPassword) && !validations.isEmpty(edtRePassword)){
                    if (validations.isEmail(edtEmail)){
                        if (validations.isValidPhoneNumber(edtPhone)){
                            if (validations.isSamePassword(edtPassword,edtRePassword)){
                                callRegistration();
                            }else{
                                new MakeToast(R.string.samePass);
                            }
                        }else{
                            new MakeToast(R.string.phoneValidation);
                        }
                    }else{
                        new MakeToast(R.string.emailValidation);
                    }
                }else{
                    new MakeToast(R.string.allValidation);
                }
            }
        });
    }

    private void init() {
        tvrSignup=findViewById(R.id.tvrSignup);
        progress=findViewById(R.id.progress);
        edtName=findViewById(R.id.edtName);
        edtEmail=findViewById(R.id.edtEmail);
        edtPhone=findViewById(R.id.edtPhone);
        edtPassword=findViewById(R.id.edtPassword);
        edtRePassword=findViewById(R.id.edtRePassword);
    }
    private void callRegistration() {
        progress.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(RegistrationActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.registration, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response!=null){
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++reg","+++json "+jsonObject);
                        if (jsonObject.optInt("success")==1) {
                            JSONObject object=jsonObject.getJSONObject("data");
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.user_id,  object.optString("user_id")).apply();
                            SharedPrefs.getSharedPref().edit().putString(SharedPrefs.userSharedPrefData.var_name,  object.optString("var_name")).apply();
                            progress.setVisibility(View.GONE);
                            new MakeToast(jsonObject.optString("message"));
                            startActivity(new Intent(RegistrationActivity.this,HomeActivity.class));
                            finish();
                        }else{
                            progress.setVisibility(View.GONE);
                            new MakeToast(jsonObject.optString("message"));
                        }
                        progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "registration");
                params.put("var_mobileno", edtPhone.getText().toString());
                params.put("var_name", edtName.getText().toString());
                params.put("var_email", edtEmail.getText().toString());
                params.put("var_password", edtPassword.getText().toString());
                params.put("com_password", edtRePassword.getText().toString());
                params.put("device_id", SharedPrefs.getSharedPref().getString(SharedPrefs.tokendetail.refreshtoken,Constant.notAvailable));
                Log.d("++++reg","+++params "+params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }
}
