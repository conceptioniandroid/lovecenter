package com.marvix.lovetv.activity;

import android.content.ContextWrapper;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageView;
import android.widget.Toast;

import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.SharedPrefs;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        ImageView imageView = findViewById(R.id.bgpattern);
        imageView.getLayoutParams().height = (int) (height * 0.20);
        imageView.requestLayout();

        ImageView centerlogo = findViewById(R.id.centerlogo);
        centerlogo.getLayoutParams().width = (int) (width * 0.40);
        centerlogo.requestLayout();


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id,
                        Constant.notAvailable).equalsIgnoreCase(Constant.notAvailable)) {

                    startActivity(new Intent(SplashActivity.this, HomeActivity.class));
                    finish();
                } else {
                    startActivity(new Intent(SplashActivity.this, LoginActivity.class));
                    finish();
                }
            }
        }, 3000);
    }
}
