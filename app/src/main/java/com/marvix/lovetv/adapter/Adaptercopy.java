package com.marvix.lovetv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubePlayerView;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.fragment.VideoFragment;
import com.marvix.lovetv.listener.OnLoadMoreListener;
import com.marvix.lovetv.model.PlaylistItem;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.Config;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

import java.util.ArrayList;

import static android.provider.MediaStore.Video.Thumbnails.VIDEO_ID;

public class Adaptercopy extends RecyclerView.Adapter<Adaptercopy.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<PlaylistItem> playlistItems;
    VideoFragment videoFragment;

    public Adaptercopy(RecyclerView recyclerView, ArrayList<PlaylistItem> playlistItems, Context context, VideoFragment videoFragment){
        this.playlistItems = playlistItems;
        this.videoFragment = videoFragment;
        this.context = context;
    }//
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_video_slider, parent, false);
        return new Adaptercopy.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, final int i) {
        final Adaptercopy.Holder holder1 = (Adaptercopy.Holder) holder;
        holder1.tvmTitle.setText(playlistItems.get(i).getItemSnippet().getTitle());
        holder1.tvrDescp.setText(playlistItems.get(i).getItemSnippet().getDescription());
//        final YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();
//
//        FragmentTransaction transaction = videoFragment.getChildFragmentManager().beginTransaction();
//        transaction.add(R.id.youtube_view, youTubePlayerFragment).commit();

//        holder.youtube_player_view.initialize(Config.YOUTUBE_API_KEY_NEW, new YouTubePlayer.OnInitializedListener() {
//            @Override
//            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean wasRestored) {
//                if (!wasRestored) {
//                    youTubePlayer.cueVideo(playlistItems.get(i).getItemSnippet().getSnippetResourceId().getVideoId());
//                }
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//
//            }
//        });










//        holder.youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY_NEW, new YouTubePlayer.OnInitializedListener() {
//
//            @Override
//            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
//                if (!wasRestored) {
//                    player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
//                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
//                    player.setShowFullscreenButton(true);
//                    //  player.cuePlaylist("PLkGHGHvUa3ahZE2TLmtxV3KXxyum55Q3y");
//                    // player.setFullscreen(true);
//                    player.cueVideo(playlistItems.get(i).getItemSnippet().getSnippetResourceId().getVideoId());
//                    Log.d("++++video", "++ " + i + playlistItems.get(i).getItemSnippet().getSnippetResourceId().getVideoId() + "+++" + playlistItems.size());
//                    player.play();
//
//                    //player.loadVideo(video,0);
//                }
//            }
//
//            @Override
//            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
//                String errorMessage = error.toString();
//                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
//                Log.d("errorMessage:", errorMessage);
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return playlistItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        TextviewRegular tvrDescp;
        TextviewMedium tvmTitle;
        YouTubePlayerView youtube_player_view;
        YouTubePlayerSupportFragment youTubePlayerFragment;
        public Holder(@NonNull View itemView) {
            super(itemView);
            tvrDescp = itemView.findViewById(R.id.tvrDescp);
            tvmTitle = itemView.findViewById(R.id.tvmTitle);
            youtube_player_view = itemView.findViewById(R.id.youtube_player_view);

            youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

            FragmentTransaction transaction = videoFragment.getChildFragmentManager().beginTransaction();
            transaction.add(R.id.youtube_view, youTubePlayerFragment).commit();
        }
    }
}
