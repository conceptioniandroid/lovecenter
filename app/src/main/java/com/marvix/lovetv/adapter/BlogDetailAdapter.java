package com.marvix.lovetv.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;


import com.android.volley.toolbox.ImageLoader;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.ChatModel;
import com.marvix.lovetv.utils.TextviewRegular;

import java.util.ArrayList;


public class BlogDetailAdapter extends RecyclerView.Adapter<BlogDetailAdapter.BlogDetailViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<ChatModel> chatModelArrayList = new ArrayList<>();

    public BlogDetailAdapter(ArrayList<ChatModel> chatModelArrayList) {
        this.chatModelArrayList = chatModelArrayList;
    }

    @Override
    public BlogDetailViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_comment, parent, false);
        return new BlogDetailViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final BlogDetailViewHolder holder, final int position) {
        holder.user_nametvr.setText(chatModelArrayList.get(position).getVar_name());
        holder.chat_date_timetvr.setText(chatModelArrayList.get(position).getDate()+" "+"at"+ " " + chatModelArrayList.get(position).getTime());

        String text = chatModelArrayList.get(position).getVar_massage();

        if (text.length() >= 80) {
            text = text.substring(0, 79) + "...";
            holder.chat_texttvr.setText(Html.fromHtml(text + "<font color='#1da1f3'>View More</font>"));
        } else {
            holder.chat_texttvr.setText(chatModelArrayList.get(position).getVar_massage());
        }

        holder.chat_texttvr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String newsText = chatModelArrayList.get(position).getVar_massage();
                if (newsText.length() >= 80) {
                    if (holder.isSelected) {
                        holder.isSelected = false;
                        newsText = newsText.substring(0, 79) + "...";
                        holder.chat_texttvr.setText(Html.fromHtml(newsText + "<font color='#1da1f3'>View More</font>"));
                    } else {
                        holder.isSelected = true;
                        newsText = newsText + "...";
                        holder.chat_texttvr.setText(Html.fromHtml(newsText + "<font color='#1da1f3'>View Less</font>"));
                    }
                }
            }
        });

        if (chatModelArrayList.get(position).getChatReplyModelArrayList().size() == 0) {
            holder.view_all_repliesll.setVisibility(View.GONE);
            holder.noreply.setVisibility(View.VISIBLE);
        } else {
            holder.noreply.setVisibility(View.GONE);
            holder.hideshowtv.setText("Show Previous Replies");
            holder.view_all_repliesll.setVisibility(View.VISIBLE);
            holder.commentreplyrecycler.setVisibility(View.GONE);
        }

        holder.view_all_repliesll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.isshown){
                    holder.isshown = false;
                    holder.commentreplyrecycler.setVisibility(View.VISIBLE);
                    holder.hideshowtv.setText("Hide Previous Replies");
                }else {
                    holder.isshown = true;
                    holder.commentreplyrecycler.setVisibility(View.GONE);
                    holder.hideshowtv.setText("Show Previous Replies");
                }
            }
        });

        LinearLayoutManager layoutManager = new LinearLayoutManager(context) {
            @Override
            public boolean canScrollVertically() {
                return false;
            }
        };
        holder.commentreplyrecycler.setLayoutManager(layoutManager);
        holder.commentReplyAdapter = new CommentReplyAdapter(chatModelArrayList.get(position).getChatReplyModelArrayList());
        holder.commentreplyrecycler.setAdapter(holder.commentReplyAdapter);
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(chatModelArrayList.get(position).getVar_image()).apply(options).into(holder.ivComment);
    }

    @Override
    public int getItemCount() {
        return chatModelArrayList.size();
    }

    public class BlogDetailViewHolder extends RecyclerView.ViewHolder {
        LinearLayout replyll;
        RecyclerView commentreplyrecycler;
        TextviewRegular chat_texttvr,chat_date_timetvr,user_nametvr,hideshowtv;
        LinearLayout reply;
        LinearLayout noreply;
        LinearLayout view_all_repliesll;

        boolean isSelected;
        boolean isshown = false;

        CommentReplyAdapter commentReplyAdapter;
        RoundedImageView ivComment;
        public BlogDetailViewHolder(View itemView) {
            super(itemView);
            ivComment=itemView.findViewById(R.id.ivComment);
            commentreplyrecycler=itemView.findViewById(R.id.commentreplyrecycler);
            chat_texttvr=itemView.findViewById(R.id.chat_texttvr);
            chat_date_timetvr=itemView.findViewById(R.id.chat_date_timetvr);
            user_nametvr=itemView.findViewById(R.id.user_nametvr);
            hideshowtv=itemView.findViewById(R.id.hideshowtv);
            noreply=itemView.findViewById(R.id.noreply);
            view_all_repliesll=itemView.findViewById(R.id.view_all_repliesll);
        }
    }
}
