package com.marvix.lovetv.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.ChatReplyModel;
import com.marvix.lovetv.utils.TextviewRegular;


import java.util.ArrayList;


/**
 * Created by admin on 12/27/2017.
 */

public class CommentReplyAdapter extends RecyclerView.Adapter<CommentReplyAdapter.CommentReplyViewHolder> {

    Context context;
    LayoutInflater layoutInflater;
    ArrayList<ChatReplyModel> chatReplyModelArrayList = new ArrayList<>();

    public CommentReplyAdapter(ArrayList<ChatReplyModel> chatReplyModelArrayList){
        this.chatReplyModelArrayList = chatReplyModelArrayList;
    }

    @Override
    public CommentReplyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_comment_reply, parent, false);
        return new CommentReplyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentReplyViewHolder holder, int position) {
        Log.d("+++chatrepleadapter","++ ");
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(chatReplyModelArrayList.get(position).getVar_image()).apply(options).into(holder.ivReply);
        holder.user_nametvr.setText(chatReplyModelArrayList.get(position).getNew_name());
        holder.chat_date_timetvr.setText(chatReplyModelArrayList.get(position).getDate()+" "+"at"+ " " + chatReplyModelArrayList.get(position).getTime());
        holder.chat_text.setText(Html.fromHtml("<b>"+chatReplyModelArrayList.get(position).getNew_message()+"</b>"));
    }

    @Override
    public int getItemCount() {
        return chatReplyModelArrayList.size();
    }

    public class CommentReplyViewHolder extends RecyclerView.ViewHolder {
        TextviewRegular user_nametvr,chat_date_timetvr,chat_text;
        RoundedImageView ivReply;
        public CommentReplyViewHolder(View itemView) {
            super(itemView);
            ivReply=itemView.findViewById(R.id.ivReply);
            user_nametvr=itemView.findViewById(R.id.user_nametvr);
            chat_date_timetvr=itemView.findViewById(R.id.chat_date_timetvr);
            chat_text=itemView.findViewById(R.id.chat_text);
        }
    }
}
