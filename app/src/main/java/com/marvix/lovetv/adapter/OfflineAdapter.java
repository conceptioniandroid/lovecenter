package com.marvix.lovetv.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;
import com.marvix.lovetv.activity.MusicPlayerActivity;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.TextviewRegular;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import dm.audiostreamer.MediaMetaData;

public class OfflineAdapter extends RecyclerView.Adapter<OfflineAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    private List<TopAudioModel> musicList;

    public OfflineAdapter(Context context, List<TopAudioModel> musicList) {
        this.context = context;
        this.musicList = musicList;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.rowitem_topaudio, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(musicList.get(i).getVar_image()).apply(options).into(holder.imageView1);
        holder.tvrTitleAudio.setText(musicList.get(i).getVar_title());
        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                List<MediaMetaData> listOfSongs = new ArrayList<>();

                for (int j = 0; j < musicList.size(); j++) {
                    if (HomeActivity.mp != null)
                        HomeActivity.mp.release();
                    MediaMetaData mediaMetaData = new MediaMetaData();
                    mediaMetaData.setMediaId(musicList.get(j).getInt_glcode());
                    mediaMetaData.setMediaUrl(context.getApplicationInfo().dataDir + "/" + musicList.get(j).getVar_link());
                    mediaMetaData.setMediaTitle(musicList.get(j).getVar_title());
                    mediaMetaData.setMediaArtist(musicList.get(j).getVar_description());
                    mediaMetaData.setMediaAlbum(musicList.get(j).getVar_title());
                    mediaMetaData.setMediaComposer(musicList.get(j).getVar_title());
                    mediaMetaData.setMediaDuration(musicList.get(j).getAudio_Length());
                    mediaMetaData.setMediaArt(musicList.get(j).getVar_image());
                    mediaMetaData.setMediaLength(musicList.get(j).getAudio_Length());

                    Log.d("Mydaya", "onClick: "+context.getApplicationInfo().dataDir + "/" + musicList.get(j).getVar_link());

                    listOfSongs.add(mediaMetaData);
                }

                Intent in = new Intent(context, MusicPlayerActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) listOfSongs);
                in.putExtra("BUNDLE", args);
                in.putExtra("position", i + "");
                context.startActivity(in);

            }
        });

    }


    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public int getItemCount() {
        return musicList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RoundedImageView imageView1;
        TextviewRegular tvrTitleAudio;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tvrTitleAudio = itemView.findViewById(R.id.tvrTitleAudio);
            imageView1 = itemView.findViewById(R.id.imageView1);

        }
    }

}
