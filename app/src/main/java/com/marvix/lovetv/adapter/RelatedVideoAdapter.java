package com.marvix.lovetv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;

import com.marvix.lovetv.R;
import com.marvix.lovetv.model.RelatedVideoModel;
import com.marvix.lovetv.utils.TextviewRegular;

public class RelatedVideoAdapter extends RecyclerView.Adapter<RelatedVideoAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<RelatedVideoModel>relatedVideoModels;

    public RelatedVideoAdapter( ArrayList<RelatedVideoModel>relatedVideoModels){
        this.relatedVideoModels=relatedVideoModels;
    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.rowitem_relatedvideo, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(relatedVideoModels.get(i).getVar_image()).apply(options).into(holder.imageView1);
        holder.tvrRtitle.setText(relatedVideoModels.get(i).getVar_title());
        holder.tvrRDesc.setText(relatedVideoModels.get(i).getVar_description());
    }

    @Override
    public int getItemCount() {
        return relatedVideoModels.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RoundedImageView imageView1;
        TextviewRegular tvrRtitle,tvrRDesc;
        public Holder(@NonNull View itemView) {
            super(itemView);
            tvrRDesc = itemView.findViewById(R.id.tvrRDesc);
            tvrRtitle = itemView.findViewById(R.id.tvrRtitle);
            imageView1 = itemView.findViewById(R.id.imageView1);
        }
    }
}
