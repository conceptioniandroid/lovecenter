package com.marvix.lovetv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;

import java.util.ArrayList;
import java.util.List;

import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.TextviewRegular;

import dm.audiostreamer.MediaMetaData;

public class TopAudioAdapter extends RecyclerView.Adapter<TopAudioAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<MediaMetaData> topAudioModels;
    private List<MediaMetaData> musicList;

    public TopAudioAdapter(Context context, List<MediaMetaData> musicList){
        this.context = context;
        this.musicList = musicList;
        this.topAudioModels=topAudioModels;
    }//
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.rowitem_topaudio, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, final int i) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        Glide.with(context).load(musicList.get(i).getMediaArt()).apply(options).into(holder.imageView1);
        holder.tvrTitleAudio.setText(musicList.get(i).getMediaTitle());
        holder.imageView1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItemListener != null) {
                    listItemListener.onItemClickListener(musicList.get(i),i+"");
                }
            }
        });
        holder.dwdld.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listItemListener != null) {
                    listItemListener.onItemClickListener(musicList.get(i),"download");
                }
            }
        });
    }
    public void refresh(List<MediaMetaData> musicList) {
        if (this.musicList != null) {
            this.musicList.clear();
        }
        this.musicList.addAll(musicList);
        notifyDataSetChanged();
    }

    public void notifyPlayState(MediaMetaData metaData) {
        if (this.musicList != null && metaData != null) {
            int index = this.musicList.indexOf(metaData);
            //TODO SOMETIME INDEX RETURN -1 THOUGH THE OBJECT PRESENT IN THIS LIST
            if (index == -1) {
                for (int i = 0; i < this.musicList.size(); i++) {
                    if (this.musicList.get(i).getMediaId().equalsIgnoreCase(metaData.getMediaId())) {
                        index = i;
                        break;
                    }
                }
            }
            if (index > 0 && index < this.musicList.size()) {
                this.musicList.set(index, metaData);
            }
        }
        notifyDataSetChanged();
    }

    public void add(List<MediaMetaData> musicList) {
        int previousDataSize = this.musicList.size();
        this.musicList.addAll(musicList);
        notifyItemRangeInserted(previousDataSize, musicList.size());
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }
    @Override
    public int getItemCount() {
        return musicList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RoundedImageView imageView1;
        TextviewRegular tvrTitleAudio;
        LinearLayout topplayaudioll;
        ImageView dwdld;
        public Holder(@NonNull View itemView) {
            super(itemView);
            tvrTitleAudio=itemView.findViewById(R.id.tvrTitleAudio);
            imageView1=itemView.findViewById(R.id.imageView1);
            topplayaudioll=itemView.findViewById(R.id.topplayaudioll);
            dwdld=itemView.findViewById(R.id.dwdld);

        }
    }
    public void setListItemListener(TopAudioAdapter.ListItemListener listItemListener) {
        this.listItemListener = listItemListener;
    }

    public ListItemListener listItemListener;

    public interface ListItemListener {
        void onItemClickListener(MediaMetaData media,String type);
    }
}
