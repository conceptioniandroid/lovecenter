package com.marvix.lovetv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.PlaylistItem;
import com.marvix.lovetv.model.YouTubeModel;
import com.marvix.lovetv.utils.TextviewRegular;

import java.util.ArrayList;

public class VideoPlaylistAdapter extends RecyclerView.Adapter<VideoPlaylistAdapter.Holder> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<PlaylistItem> playlistItems;

    public VideoPlaylistAdapter(ArrayList<PlaylistItem> playlistItems){
        this.playlistItems=playlistItems;
    }
    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        context = parent.getContext();
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.rowitem_videoplaylist, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int i) {
        RequestOptions options = new RequestOptions()
                .centerCrop()
                .placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .priority(Priority.HIGH);
        if (!playlistItems.get(i).getItemSnippet().getSnippetThumbnail().getDefault().getUrl().isEmpty())
        Glide.with(context).load(playlistItems.get(i).getItemSnippet().getSnippetThumbnail().getDefault().getUrl()).apply(options).into(holder.imageView1);
        holder.tvrTitle.setText(playlistItems.get(i).getItemSnippet().getTitle());
        holder.tvrDesc.setText(playlistItems.get(i).getItemSnippet().getDescription());

    }

    @Override
    public int getItemCount() {
        return playlistItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        RoundedImageView imageView1;
        TextviewRegular tvrTitle,tvrDesc;
        CardView cardVideo;
        public Holder(@NonNull View itemView) {
            super(itemView);
            cardVideo = itemView.findViewById(R.id.cardVideo);
            imageView1 = itemView.findViewById(R.id.imageView1);
            tvrTitle = itemView.findViewById(R.id.tvrTitle);
            tvrDesc = itemView.findViewById(R.id.tvrDesc);
        }
    }
}
