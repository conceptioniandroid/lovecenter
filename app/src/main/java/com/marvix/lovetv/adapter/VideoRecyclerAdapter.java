package com.marvix.lovetv.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.marvix.lovetv.R;
import com.marvix.lovetv.fragment.VideoFragment;
import com.marvix.lovetv.listener.OnLoadMoreListener;
import com.marvix.lovetv.model.PlaylistItem;
import com.marvix.lovetv.utils.Config;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

import java.util.ArrayList;

public class VideoRecyclerAdapter extends AbstractRecyclerViewFooterAdapter<PlaylistItem> {
    Context context;
    LayoutInflater layoutInflater;
    ArrayList<PlaylistItem> playlistItems;
    VideoFragment videoFragment;
    String video;

    public VideoRecyclerAdapter(RecyclerView recyclerView, ArrayList<PlaylistItem> playlistItems, OnLoadMoreListener onLoadMoreListener, Context context, VideoFragment videoFragment) {
        super(recyclerView, playlistItems, onLoadMoreListener);
        this.playlistItems = playlistItems;
        this.videoFragment = videoFragment;
        this.context = context;
    }

    @Override
    protected RecyclerView.ViewHolder onCreateBasicItemViewHolder(ViewGroup parent, int viewType) {
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.row_item_video_slider, parent, false);
        return new Holder(view);
    }

    @Override
    protected void onBindBasicItemView(RecyclerView.ViewHolder holder, final int i) {
        final Holder holder1 = (Holder) holder;
        holder1.tvmTitle.setText(playlistItems.get(i).getItemSnippet().getTitle());
        holder1.tvrDescp.setText(playlistItems.get(i).getItemSnippet().getDescription());

        final YouTubePlayerSupportFragment youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = videoFragment.getChildFragmentManager().beginTransaction();
        transaction.add(R.id.youtube_view, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY_NEW, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.setFullscreenControlFlags(YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION);
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    player.setShowFullscreenButton(true);
                    //  player.cuePlaylist("PLkGHGHvUa3ahZE2TLmtxV3KXxyum55Q3y");
                    // player.setFullscreen(true);
                    player.cueVideo(playlistItems.get(i).getItemSnippet().getSnippetResourceId().getVideoId());
                    Log.d("++++video", "++ " + playlistItems.get(i).getItemSnippet().getSnippetResourceId().getVideoId() + "+++" + playlistItems.size());
                    player.play();

                    //player.loadVideo(video,0);
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                String errorMessage = error.toString();
                Toast.makeText(context, errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }
        });
    }


    @Override
    public int getItemCount() {
        return playlistItems.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextviewRegular tvrDescp;
        TextviewMedium tvmTitle;

        public Holder(@NonNull View itemView) {
            super(itemView);
            tvrDescp = itemView.findViewById(R.id.tvrDescp);
            tvmTitle = itemView.findViewById(R.id.tvmTitle);
        }
    }
}
