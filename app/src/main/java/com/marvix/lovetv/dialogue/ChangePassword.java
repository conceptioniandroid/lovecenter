package com.marvix.lovetv.dialogue;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewRegular;
import com.marvix.lovetv.utils.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePassword {

    Context context;
    Dialog dialog;

    Button changepassll,okbtn;
    EditText oldpass,newpass,confirmpass;
    LinearLayout successll,changepass;
    TextviewRegular successtvr;
    ProgressBar progress;

    public ChangePassword(Context context){
        this.context = context;
    }


    public void ShowChangeDialogue(){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.change_pass);
        dialog.setCancelable(true);
        dialog.setTitle("Change Password?");
        dialog.show();

        changepassll = dialog.findViewById(R.id.changepassll);
        okbtn = dialog.findViewById(R.id.okbtn);
        oldpass = dialog.findViewById(R.id.oldpass);
        newpass = dialog.findViewById(R.id.newpass);
        confirmpass = dialog.findViewById(R.id.confirmpass);
        successll = dialog.findViewById(R.id.successll);
        successtvr = dialog.findViewById(R.id.successtvr);
        progress = dialog.findViewById(R.id.progress);
        changepass = dialog.findViewById(R.id.changepass);


        changepassll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validations validations = new Validations();
                if (!validations.isEmpty(oldpass)) {
                    if (!validations.isEmpty(newpass)){
                        if (!validations.isEmpty(confirmpass)){
                            if (validations.isValidPassword(oldpass)){
                                if (validations.isValidPassword(newpass)){
                                    if (!oldpass.getText().toString().equalsIgnoreCase(newpass.getText().toString())){
                                            changepassll.setEnabled(false);
//                                        spin_kit.setVisibility(View.VISIBLE);
                                            CallForgetPassApi();

                                    }else {
                                        new MakeToast("Current password and new password must not be same");
                                    }
                                }else {
                                    new MakeToast(context.getString(R.string.entervalidpass));
                                }
                            }else {
                                new MakeToast(context.getString(R.string.entervalidpass));
                            }
                        }else {
                            new MakeToast("Please enter confirm password");
                        }
//
                    }else {
                        new MakeToast("Please enter new password");
                    }

                } else {
                    new MakeToast("Please enter current password");
                }
            }
        });

        okbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    public void CallForgetPassApi(){

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest strRequest = new StringRequest(Request.Method.POST, Constant.ChangePassword,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
//                                spin_kit.setVisibility(View.GONE);
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("success").equalsIgnoreCase("1")){
                                    changepass.setVisibility(View.GONE);
                                    successll.setVisibility(View.VISIBLE);
                                    successtvr.setText(jsonObject.optString("message"));
                                }else {
                                    changepass.setVisibility(View.GONE);
                                    successll.setVisibility(View.VISIBLE);
                                    successtvr.setText(jsonObject.optString("message"));
                                    new MakeToast(jsonObject.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            changepass.setEnabled(true);
                            new MakeToast("Please try after some time");
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        changepass.setEnabled(true);
                        new MakeToast("Please Try After Some TIme");
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API","change_password");
                params.put("user_id",SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id,Constant.notAvailable));
                params.put("old_password",oldpass.getText().toString());
                params.put("new_password",newpass.getText().toString());
                params.put("com_password",confirmpass.getText().toString());
                return params;
            }
        };
        queue.add(strRequest);
    }
}
