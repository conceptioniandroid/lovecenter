package com.marvix.lovetv.dialogue;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.TextviewRegular;
import com.marvix.lovetv.utils.Validations;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgetPassword {

    Context context;
    Dialog dialog;
    LinearLayout successLL,forgetLL,llEmail;
    TextviewRegular successmessage;
    Button dialog_send_email;
    EditText edtEmail;

    public ForgetPassword(Context context){
        this.context = context;
    }

    public void ShowForgetDialogue(){
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.forget_dialogue);
        dialog.setCancelable(true);
        dialog.setTitle("Forgot Password?");

         edtEmail = dialog.findViewById(R.id.edtEmail);
        dialog_send_email = dialog.findViewById(R.id.dialog_send_email);
        successLL = dialog.findViewById(R.id.successLL);
        successmessage = dialog.findViewById(R.id.successmessage);
        Button ok = dialog.findViewById(R.id.ok);
        llEmail = dialog.findViewById(R.id.llEmail);
        forgetLL = dialog.findViewById(R.id.forgetLL);

        dialog_send_email.setEnabled(true);

        dialog_send_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Validations validations = new Validations();
                if (!validations.isEmpty(edtEmail)) {
                        dialog_send_email.setEnabled(false);
                        CallForgetPassApi();
                } else {
                    new MakeToast(R.string.validEmail);
                }
            }
        });

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();

    }


    public void CallForgetPassApi(){

        RequestQueue queue = Volley.newRequestQueue(context);

        StringRequest strRequest = new StringRequest(Request.Method.POST, Constant.Forgotpassword,
                new Response.Listener<String>()
                {
                    @Override
                    public void onResponse(String response) {
                        if (response != null) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);
                                if (jsonObject.getString("success").equalsIgnoreCase("1")){
                                    forgetLL.setVisibility(View.GONE);
                                    successLL.setVisibility(View.VISIBLE);
                                    successmessage.setText(jsonObject.optString("message"));
                                }else {
                                    new MakeToast(jsonObject.getString("message"));
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else {
                            dialog_send_email.setEnabled(true);
                            new MakeToast("No data");
                        }
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error)
                    {
                        dialog_send_email.setEnabled(true);
                        new MakeToast("Please Try After Some TIme");
                    }
                })
        {
            @Override
            protected Map<String, String> getParams()
            {
                Map<String, String> params = new HashMap<String, String>();
                params.put("var_email",edtEmail.getText().toString());
                params.put("API","forgot_password");
                return params;
            }
        };
        queue.add(strRequest);
    }
}
