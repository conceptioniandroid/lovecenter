package com.marvix.lovetv.firebase;


import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.JsonObject;
import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.SharedPrefs;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);

        Log.d("++++++push","+++++"+remoteMessage.getData().toString());

        if (remoteMessage.getData() != null) {

                JSONObject object = new JSONObject(remoteMessage.getData());

                Log.d("++++++push","+++++if "+remoteMessage.getData().toString());
              //  performheadsUP();

        }
    }
    @TargetApi(Build.VERSION_CODES.O)
    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void performheadsUP() {
        //RemoteViews remoteViews = new RemoteViews(getPackageName(),R.layout.rowitem_nav);
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        Intent notificationIntent = null;
        if (!SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id, Constant.notAvailable).equalsIgnoreCase(Constant.notAvailable)) {
           // notificationIntent = new Intent(MyFirebaseMessagingService.this, MenuActivity.class);
          //  notificationIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        }

        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
            Notification notification = new NotificationCompat.Builder(this)
                    .setCategory(Notification.CATEGORY_PROMO)
                    .setContentTitle(getString(R.string.app_name))
                    //.setContentText(text)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                   // .addAction(android.R.drawable.ic_menu_view, "View details", contentIntent)
                    .setContentIntent(contentIntent)
                    .setSound(uri)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setPriority(NotificationManager.IMPORTANCE_MAX)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setChannelId(CHANNEL_ID)
                    .setVibrate(new long[0]).build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            try {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    notificationManager.createNotificationChannel(mChannel);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            notificationManager.notify(0, notification);
        }else{
            Notification notification = new NotificationCompat.Builder(this)
                    //.setContent(remoteViews)
                    .setCategory(Notification.CATEGORY_PROMO)
                    .setContentTitle(getString(R.string.app_name))
                   // .setContentText(text)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher))
                    .setAutoCancel(true)
                   // .addAction(android.R.drawable.ic_menu_view, "View details", contentIntent)
                    .setContentIntent(contentIntent)
                    .setSound(uri)
                    .setDefaults(Notification.DEFAULT_ALL)
                    .setPriority(NotificationManager.IMPORTANCE_HIGH)
                    .setPriority(NotificationManager.IMPORTANCE_MAX)
                    .setPriority(Notification.PRIORITY_HIGH)
                    .setVibrate(new long[0]).build();
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(0, notification);
        }
    }
}