package com.marvix.lovetv.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;
import com.marvix.lovetv.activity.MusicPlayerActivity;
import com.marvix.lovetv.adapter.RecentlyAddedAudioAdapter;
import com.marvix.lovetv.adapter.TopAudioAdapter;
import com.marvix.lovetv.model.RecentAudioModel;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.ItemOffsetDecoration;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.TextviewLight;
import com.marvix.lovetv.utils.TextviewRegular;
import com.marvix.lovetv.widgets.PlayPauseView;
import com.paginate.Paginate;
import com.paginate.recycler.LoadingListItemCreator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import dm.audiostreamer.MediaMetaData;


public class AudioFragment extends Fragment implements View.OnClickListener {
    RecyclerView rvRecentAdd, rvTopAudio;
    TopAudioAdapter topAudioAdapter;
    View view;
    LinearLayoutManager linearLayoutManager;
    GridLayoutManager gridLayoutManager;
    ProgressBar progress;
    TextviewRegular tvrNodata, audionametvr;
    TextviewLight audiodesctvl;
    ArrayList<RecentAudioModel> recentAudioModels = new ArrayList<>();
    ArrayList<TopAudioModel> topAudioModels = new ArrayList<>();
    ImageView playbtniv;
    SeekBar audioprogress;
    ProgressBar playprogress;
    RoundedImageView imageView1;
    Toolbar toolbar;
    String TAG = "AUDIOFRAGMENT";
    String playingFrom = "R";
    PlayPauseView btn_play;
    TextviewRegular timetvr;
    RecentlyAddedAudioAdapter recentlyAddedAudioAdapter;
    RelativeLayout mainrl;
    Context context;
    int page = 1;
    Handler handler;
    private List<MediaMetaData> listOfSongs = new ArrayList<MediaMetaData>();
    private List<MediaMetaData> listOfSongsTop = new ArrayList<MediaMetaData>();
    private Paginate paginate;
    private boolean loading = false;
    private ImageView download;


    private Runnable fakeCallback = new Runnable() {
        @Override
        public void run() {
            callaudioList();
        }
    };
    Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            page++;
            loading = true;
            handler.post(fakeCallback);
        }

        @Override
        public boolean isLoading() {
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            return page == 10000;
        }
    };
    Context mContext;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        mContext = container.getContext();

        view = inflater.inflate(R.layout.fragment_audio, container, false);
        context = container.getContext();
        progress = view.findViewById(R.id.progress);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        mainrl = view.findViewById(R.id.mainrl);
        download = view.findViewById(R.id.download);
        callaudioList();
        return view;
    }

    private void clicks() {
        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, "Hello", Toast.LENGTH_SHORT).show();

            }
        });
        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new MydownloadsFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.addToBackStack("");
                ft.replace(R.id.content, fragment);
                ft.commit();
            }
        });
    }

    @SuppressLint("NewApi")
    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        rvRecentAdd = view.findViewById(R.id.rvRecentAdd);
        rvTopAudio = view.findViewById(R.id.rvTopAudio);
        playbtniv = view.findViewById(R.id.playbtniv);
        audioprogress = view.findViewById(R.id.audioprogress);
        playprogress = view.findViewById(R.id.playprogress);
        audionametvr = view.findViewById(R.id.audionametvr);
        audiodesctvl = view.findViewById(R.id.audiodesctvl);
        imageView1 = view.findViewById(R.id.imageView1);
        btn_play = view.findViewById(R.id.btn_play);
        timetvr = view.findViewById(R.id.timetvr);

        handler = new Handler();

        linearLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvRecentAdd.setLayoutManager(linearLayoutManager);

        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        rvTopAudio.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(context, R.dimen.item_offset);
        rvTopAudio.addItemDecoration(itemDecoration);

        Log.d(TAG, "init: " + gridLayoutManager.findFirstVisibleItemPosition() + gridLayoutManager.findFirstCompletelyVisibleItemPosition());

        recentlyAddedAudioAdapter = new RecentlyAddedAudioAdapter(getActivity(), new ArrayList<MediaMetaData>());
        recentlyAddedAudioAdapter.setListItemListener(new RecentlyAddedAudioAdapter.ListItemListener() {
            @Override
            public void onItemClickListener(MediaMetaData media, String type) {
                if (HomeActivity.mp != null)
                    HomeActivity.mp.release();

                Intent in = new Intent(context, MusicPlayerActivity.class);
                Bundle args = new Bundle();
                args.putSerializable("ARRAYLIST", (Serializable) listOfSongs);
                in.putExtra("BUNDLE", args);
                in.putExtra("position", type);
                startActivity(in);
            }
        });

        rvRecentAdd.setAdapter(recentlyAddedAudioAdapter);

        if (page == 1) {
            topAudioAdapter = new TopAudioAdapter(getActivity(), new ArrayList<MediaMetaData>());
            topAudioAdapter.setListItemListener(new TopAudioAdapter.ListItemListener() {
                @Override
                public void onItemClickListener(MediaMetaData media, String type) {
                    if (HomeActivity.mp != null)
                        HomeActivity.mp.release();

                    Intent in = new Intent(context, MusicPlayerActivity.class);
                    Bundle args = new Bundle();
                    args.putSerializable("ARRAYLIST", (Serializable) listOfSongsTop);
                    in.putExtra("BUNDLE", args);
                    in.putExtra("position", type);
                    startActivity(in);
                }
            });
            rvTopAudio.setAdapter(topAudioAdapter);

            paginate = Paginate.with(rvTopAudio, callbacks)
                    .setLoadingTriggerThreshold(2)
                    .addLoadingListItem(true)
                    .setLoadingListItemCreator(new CustomLoadingListItemCreator())
                    .build();
        } else {
            topAudioAdapter.add(new ArrayList<MediaMetaData>());
            loading = false;
        }

    }

    private void callaudioList() {
        progress.setVisibility(View.VISIBLE);
        tvrNodata.setVisibility(View.GONE);
        RequestQueue requestQueue = Volley.newRequestQueue(context);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.audioList, new Response.Listener<String>() {
            @SuppressLint("NewApi")
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        if (jsonObject.optInt("success") == 1) {
                            recentAudioModels.clear();
                            topAudioModels.clear();
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.GONE);
                            JSONArray jsonArray = jsonObject.getJSONArray("recent_audio");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                RecentAudioModel recentAudioModel = new RecentAudioModel();
                                JSONObject object = jsonArray.getJSONObject(i);
                                recentAudioModel.setInt_glcode(object.optString("int_glcode"));
                                recentAudioModel.setVar_image(object.optString("var_image"));
                                recentAudioModel.setVar_link(object.optString("var_link"));
                                recentAudioModel.setVar_title(object.optString("var_title"));
                                recentAudioModel.setVar_description(object.optString("var_description"));
//                                recentAudioModel.setAudio_len(object.optString("audio_len"));
                                recentAudioModel.setAudio_len(String.valueOf(converttosecond(object.optString("audio_len"))));
                                recentAudioModel.setAudio_Length(object.optString("audio_len"));
                                if (!object.optString("var_link").equalsIgnoreCase(""))
                                    recentAudioModels.add(recentAudioModel);
                            }

//                            rvRecentAdd.setAdapter(recentlyAddedAudioAdapter);

                            listOfSongs.clear();
                            for (int i = 0; i < recentAudioModels.size(); i++) {
                                MediaMetaData metaData = new MediaMetaData();
                                metaData.setMediaId(recentAudioModels.get(i).getInt_glcode());
                                metaData.setMediaUrl(recentAudioModels.get(i).getVar_link());
                                metaData.setMediaTitle(recentAudioModels.get(i).getVar_title());
                                metaData.setMediaArt(recentAudioModels.get(i).getVar_image());
                                metaData.setMediaArtist(recentAudioModels.get(i).getVar_description());
                                metaData.setMediaDuration(recentAudioModels.get(i).getAudio_len());
                                metaData.setMediaLength(recentAudioModels.get(i).getAudio_Length());
                                listOfSongs.add(metaData);
                            }


                            JSONArray jsonArray1 = jsonObject.getJSONArray("top_audio");
                            for (int j = 0; j < jsonArray1.length(); j++) {
                                TopAudioModel topAudioModel = new TopAudioModel();
                                JSONObject object1 = jsonArray1.getJSONObject(j);
                                topAudioModel.setInt_glcode(object1.optString("int_glcode"));
                                topAudioModel.setVar_image(object1.optString("var_image"));
                                topAudioModel.setVar_link(object1.optString("var_link"));
                                topAudioModel.setVar_title(object1.optString("var_title"));
                                topAudioModel.setVar_description(object1.optString("var_description"));
//                                topAudioModel.setAudio_len(object1.optString("audio_len"));
                                topAudioModel.setAudio_len(String.valueOf(converttosecond(object1.optString("audio_len"))));
                                topAudioModel.setAudio_Length(object1.optString("audio_len"));

                                if (!object1.optString("var_link").equalsIgnoreCase(""))
                                    topAudioModels.add(topAudioModel);

                            }
                            listOfSongsTop.clear();
                            for (int i = 0; i < topAudioModels.size(); i++) {
                                MediaMetaData metaData = new MediaMetaData();
                                metaData.setMediaId(topAudioModels.get(i).getInt_glcode());
                                metaData.setMediaUrl(topAudioModels.get(i).getVar_link());
                                metaData.setMediaTitle(topAudioModels.get(i).getVar_title());
                                metaData.setMediaArt(topAudioModels.get(i).getVar_image());
                                metaData.setMediaArtist(topAudioModels.get(i).getVar_description());
                                metaData.setMediaDuration(topAudioModels.get(i).getAudio_len());
                                metaData.setMediaLength(topAudioModels.get(i).getAudio_Length());
                                listOfSongsTop.add(metaData);
                            }
                            init();
                            clicks();
                            if (recentAudioModels.size() == 0 && topAudioModels.size() == 0) {
                                mainrl.setVisibility(View.GONE);
                                tvrNodata.setVisibility(View.VISIBLE);
                            } else {
                                tvrNodata.setVisibility(View.GONE);
                                mainrl.setVisibility(View.VISIBLE);
                                recentlyAddedAudioAdapter.refresh(listOfSongs);
                                topAudioAdapter.refresh(listOfSongsTop);

                            }

                        } else {

                            if (paginate != null) {
                                paginate.setHasMoreDataToLoad(false);
                                paginate.unbind();
                                progress.setVisibility(View.GONE);
                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                        tvrNodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "audio_list");
                params.put("page", "" + page);

                Log.d(TAG, "getParams: " + params.toString());
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    public int converttosecond(String length) {

        String[] audiosecond;
        int Minute, Second, Hour, TotalSecond = 0;
        audiosecond = length.split(":");
        if (audiosecond.length == 2) {
            Minute = Integer.parseInt(audiosecond[0]);
            Second = Integer.parseInt(audiosecond[1]);
            TotalSecond = Second + (60 * Minute);
        } else if (audiosecond.length == 3) {
            Hour = Integer.parseInt(audiosecond[0]);
            Minute = Integer.parseInt(audiosecond[1]);
            Second = Integer.parseInt(audiosecond[2]);
            TotalSecond = Second + (60 * Minute) + (3600 * Hour);
        }

        return TotalSecond;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_play:
                break;
        }
    }

    public static class CustomLoadingListItemCreator implements LoadingListItemCreator {
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.progress_item, parent, false);
            return new VH(view);
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        }
    }

    static class VH extends RecyclerView.ViewHolder {
        public VH(View itemView) {
            super(itemView);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
        super.onDestroy();
    }
}


