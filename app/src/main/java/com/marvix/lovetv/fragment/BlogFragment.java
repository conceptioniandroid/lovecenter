package com.marvix.lovetv.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.Html;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.BlogModel;
import com.marvix.lovetv.model.EventModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.DBOpenHelper;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.TextviewBold;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;


public class BlogFragment extends Fragment {
    View view;
    ViewPager viewPager;
    ArrayList<BlogModel> blogModels = new ArrayList<>();
    ProgressBar progress;
    TextviewRegular tvrNodata;
    String text = "", blogId = "";
    boolean isSelected = true;
    String comment_id = "", msg, c_id = "", url = "", imageBlog = "", titleBlog = "", blogid = "";
    int page = 1, CurrentViewpagerPosition = 0, Total_Pages;
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    Context context;
    android.support.v7.widget.Toolbar toolbar;
    SlidePageAdapter slidePageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        view = inflater.inflate(R.layout.blog_swiper, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        progress = view.findViewById(R.id.progress);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        viewPager = view.findViewById(R.id.pager);

        slidePageAdapter = new SlidePageAdapter();
        if (viewPager != null) {
            viewPager.setAdapter(slidePageAdapter);
            Log.d("TAG", "onResponse: " + CurrentViewpagerPosition);
            // viewPager.setCurrentItem(CurrentViewpagerPosition);
        }

        if (isNetConnectionAvailable())
            callBlog(page);
        else {

            String data = "";
            dbOpenHelper = new DBOpenHelper(context);
            sqLiteDatabase = dbOpenHelper.getReadableDatabase();
            data = dbOpenHelper.getBlogList();

            if (!data.equalsIgnoreCase("")) {
                Gson gson = new Gson();
                blogModels = gson.fromJson(data, new TypeToken<List<BlogModel>>() {
                }.getType());
                slidePageAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(context, "No data available", Toast.LENGTH_SHORT).show();
            }
            progress.setVisibility(View.GONE);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            blogId = bundle.getString("blogId");
        }


        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.d("TAG", "onPageSelected: " + i + "++++" + blogModels.size());
                CurrentViewpagerPosition = i;
                if (CurrentViewpagerPosition == blogModels.size() - 3) {
                    if (Total_Pages == page) {
                        Log.d("TAG{", "onPageSelected: 123" + Total_Pages + page);
                    } else {
                        page = page + 1;
                        if (isNetConnectionAvailable())
                            callBlog(page);
                    }

                }

            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    private void callBlogShare(final String id) {
        progress.setVisibility(View.VISIBLE);
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.blogShare, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++blogshare", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {
                            JSONObject object = jsonObject.getJSONObject("data");
                            url = object.getString("blog_url");
                            imageBlog = object.getString("var_image");
                            titleBlog = object.getString("var_title");

                            Intent intent = new Intent();
                            intent.setAction(Intent.ACTION_SEND);
                            intent.setType("text/plain");
                            Log.d("++++blogshare", "++url " + url);
                            intent.putExtra(Intent.EXTRA_TEXT, url);
                            startActivity(Intent.createChooser(intent, "Share"));
                        } else {
                            progress.setVisibility(View.GONE);
                            new MakeToast(jsonObject.optString("message"));
                        }
                        progress.setVisibility(View.GONE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "get_blog_url");
                params.put("blog_id", id);
                Log.d("++++blogshare", "+++params " + params);
                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void callBlog(final int Page) {
        if (page == 1) {
            progress.setVisibility(View.VISIBLE);
        }
        tvrNodata.setVisibility(View.GONE);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.blog, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++reg", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {

                            dbOpenHelper = new DBOpenHelper(context);
                            sqLiteDatabase = dbOpenHelper.getWritableDatabase();
                            dbOpenHelper.deleteBlogTable();

                            if (page == 1) {
                                blogModels.clear();
                            }
                            Total_Pages = jsonObject.optInt("total_pages");
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.GONE);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                BlogModel blogModel = new BlogModel();
                                JSONObject object = jsonArray.getJSONObject(i);
                                blogModel.setInt_glcode(object.optString("int_glcode"));
                                blogModel.setVar_title(object.optString("var_title"));
                                blogModel.setVar_image(object.optString("var_image"));
                                blogModel.setVar_categories(object.optString("var_categories"));
                                blogModel.setVar_description(object.optString("var_description"));
                                blogModel.setDate(object.optString("date"));
                                blogModel.setVar_postedby(object.optString("var_postedby"));
                                blogModel.setStaus(false);
                                blogModels.add(blogModel);
                            }

                            slidePageAdapter.notifyDataSetChanged();

                            if (blogModels.size() > 0) {
                                Gson gson = new Gson();
                                String data = gson.toJson(blogModels);
                                long id = dbOpenHelper.addBlog(data);
                            }

                            CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
                            indicator.setViewPager(viewPager);
                            if (blogId != null) {
                                for (int i = 0; i < blogModels.size(); i++) {
                                    if (blogModels.get(i).getInt_glcode().equalsIgnoreCase(blogId)) {
                                        viewPager.setCurrentItem(i);
                                    }
                                }
                            }
                        } else {
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.VISIBLE);
//                            new MakeToast(jsonObject.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                        tvrNodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "blog_list");
                params.put("page", "" + Page);

                Log.d("TAG", "getParams: " + params.toString());

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    class SlidePageAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        @Override
        public int getCount() {
            return blogModels.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @SuppressLint("NewApi")
        @Override
        public Object instantiateItem(ViewGroup container, final int i) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.row_item_blog_slider, container, false);
            RoundedImageView imageView1 = view.findViewById(R.id.imageView1);
            TextviewBold tvbTitle = view.findViewById(R.id.tvbTitle);
            TextviewMedium tvmDate = view.findViewById(R.id.tvmDate);
            TextviewMedium tvmPostedBy = view.findViewById(R.id.tvmPostedBy);
            TextviewMedium tvmCat = view.findViewById(R.id.tvmCat);
            final TextviewRegular tvrDesc = view.findViewById(R.id.tvrDesc);
            ImageView ivshare = view.findViewById(R.id.ivshare);
            ImageView ivComment = view.findViewById(R.id.ivComment);

            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);
            Glide.with(getActivity()).load(blogModels.get(i).getVar_image()).apply(options).into(imageView1);
            container.addView(view);
            tvbTitle.setText(blogModels.get(i).getVar_title());
            tvmDate.setText("Posted on " + blogModels.get(i).getDate());
            tvmPostedBy.setText("Posted by : " + blogModels.get(i).getVar_postedby());
            tvmCat.setText("Categories : " + blogModels.get(i).getVar_categories());
            text = blogModels.get(i).getVar_description();
            blogid = blogModels.get(i).getInt_glcode();

            if (text.length() >= 250) {
                text = text.substring(0, 249) + "...";

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvrDesc.setText(Html.fromHtml(text + "<font color='#647e95'>MORE</font>", Html.FROM_HTML_MODE_LEGACY));
                } else {
                    tvrDesc.setText(Html.fromHtml(text + "<font color='#647e95'>MORE</font>"));
                }


            } else {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                    tvrDesc.setText(Html.fromHtml(blogModels.get(i).getVar_description(), Html.FROM_HTML_MODE_LEGACY));
                } else {
                    tvrDesc.setText(Html.fromHtml(blogModels.get(i).getVar_description()));
                }
            }

            tvrDesc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String descText = blogModels.get(i).getVar_description();

                    if (descText.length() >= 250) {
                        if (blogModels.get(i).isStaus()) {
                            blogModels.get(i).setStaus(false);
                            descText = descText.substring(0, 249) + "...";
                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tvrDesc.setText(Html.fromHtml(descText + "<font color='#647e95'>MORE</font>", Html.FROM_HTML_MODE_LEGACY));
                            } else {
                                tvrDesc.setText(Html.fromHtml(descText + "<font color='#647e95'>MORE</font>"));
                            }

                        } else {
                            blogModels.get(i).setStaus(true);
                            Log.d("++++click", "++else " + isSelected + descText);
                            descText = descText + "...";
                            Log.d("++++click", "++else1 " + isSelected + descText);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                                tvrDesc.setText(Html.fromHtml(descText + "<font color='#647e95'>LESS</font>", Html.FROM_HTML_MODE_LEGACY));
                            } else {
                                tvrDesc.setText(Html.fromHtml(descText + "<font color='#647e95'>LESS</font>"));
                            }
                        }
                        notifyDataSetChanged();
                    }
                }
            });
            ivshare.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isNetConnectionAvailable())
                        callBlogShare(blogModels.get(i).getInt_glcode());
                    else
                        Toast.makeText(context, "Please Check your Internect Connection", Toast.LENGTH_SHORT).show();
                }
            });
            ivComment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Fragment fragment = new CommentFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction ft = fragmentManager.beginTransaction();
                    Bundle bundle = new Bundle();
                    bundle.putString("blogid", blogModels.get(i).getInt_glcode());
                    fragment.setArguments(bundle);
                    ft.replace(R.id.content, fragment);
                    ft.addToBackStack("");
                    ft.commit();

                }
            });
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }

    private boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) LoveCenter.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onDestroy() {
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
        super.onDestroy();
    }

}
