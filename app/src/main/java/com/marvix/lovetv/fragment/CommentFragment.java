package com.marvix.lovetv.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;
import com.marvix.lovetv.activity.LoginActivity;
import com.marvix.lovetv.adapter.BlogDetailAdapter;
import com.marvix.lovetv.model.ChatModel;
import com.marvix.lovetv.model.ChatReplyModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.RecyclerTouchListener;
import com.marvix.lovetv.utils.ScrollViewExt;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class CommentFragment extends Fragment {
    View view;
    RecyclerView commentrecycler;
    LinearLayoutManager linearLayoutManager;
    BlogDetailAdapter blogDetailAdapter;
    ArrayList<ChatModel> chatModelArrayList = new ArrayList<>();
    ImageView chat_send_text;
    EditText chat_text;
    String comment_type = "C", comment_id = "", msg, blogId, c_id = "";
    LinearLayout replaylayout, llComment;
    ScrollViewExt chatscroll;
    int pos;
    TextviewRegular replayname;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_comment, container, false);
        init();
        clicks();
        CallCommentList();
        return view;
    }

    private void clicks() {

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });

        chat_send_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!chat_text.getText().toString().equalsIgnoreCase("")) {
                    Log.d("++++1", "++ ");
                    if (comment_type.equalsIgnoreCase("C")) {
                        llComment.setVisibility(View.VISIBLE);
                        comment_id = "0";
                        msg = chat_text.getText().toString();
                        Calendar c = Calendar.getInstance();
                        System.out.println("Current time => " + c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        String formattedDate = df.format(c.getTime());

                        String formattedtime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());

                        ArrayList<ChatReplyModel> ch2 = new ArrayList<>();
                        ChatModel chatModel = new ChatModel();
                        chatModel.setComment_id("");
                        chatModel.setBlog_id(blogId);
                        chatModel.setUser_id(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id, Constant.notAvailable));
                        chatModel.setVar_name(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.var_name, Constant.notAvailable));
                        chatModel.setVar_massage(msg);
                        chatModel.setDate(formattedDate);
                        chatModel.setTime(formattedtime);
                        chatModel.setChatReplyModelArrayList(ch2);
                        //  chatModel.setVar_image(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.var_image,Constant.notAvailable));
                        chatModelArrayList.add(chatModel);

//                      chatModelArrayList.add(new ChatModel(ch2, "4", blogId,SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.int_glcode,Constants.notAvailable), ("Nikita"), "", formattedDate, chat_text.getText().toString()));
                        if (chatModelArrayList.size() > 1) {
                            blogDetailAdapter.notifyDataSetChanged();
                        } else {
                            blogDetailAdapter = new BlogDetailAdapter(chatModelArrayList);
                            commentrecycler.setAdapter(blogDetailAdapter);
                        }
                        chat_text.setText("");
                        sendScroll();
                        hideSoftKeyboard(CommentFragment.this);

                    } else {
                        comment_type = "C";
                        msg = chat_text.getText().toString();
                        Log.d("+++msg", "++ " + msg);

                        Calendar c = Calendar.getInstance();
                        System.out.println("Current time => " + c.getTime());
                        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
                        String formattedDate = df.format(c.getTime());

                        String formattedtime = new SimpleDateFormat("HH:mm").format(Calendar.getInstance().getTime());
                        ArrayList<ChatReplyModel> ch1 = chatModelArrayList.get(pos).getChatReplyModelArrayList();

                        ChatReplyModel chatReplyModel = new ChatReplyModel();
                        chatReplyModel.setBlog_id(blogId);
                        chatReplyModel.setUser_id(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id, Constant.notAvailable));
                        chatReplyModel.setNew_name(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.var_name, Constant.notAvailable));
                        chatReplyModel.setNew_message(msg);
                        chatReplyModel.setDate(formattedDate);
                        chatReplyModel.setTime(formattedtime);
                        //  chatReplyModel.setVar_image(SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.var_image, Constant.notAvailable));

                        ch1.add(chatReplyModel);

                        //ch1.add(new ChatReplyModel("1", blogId,SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.int_glcode,Constants.notAvailable),"","","", "", formattedDate, chat_text.getText().toString()));
                        chatModelArrayList.get(pos).setChatReplyModelArrayList(ch1);
                        chat_text.setText("");
                        replaylayout.setVisibility(View.GONE);
                        if (chatModelArrayList.size() > 1) {
                            blogDetailAdapter.notifyDataSetChanged();
                        } else {
                            blogDetailAdapter = new BlogDetailAdapter(chatModelArrayList);
                            commentrecycler.setAdapter(blogDetailAdapter);
                        }
                        hideSoftKeyboard(CommentFragment.this);
                    }
                    CallAddComment();
                } else {
                    Log.d("++++1", "++2 ");

                    new MakeToast("Please write the comment");
                }
            }
        });

    }

    private void sendScroll() {
        final Handler handler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        chatscroll.fullScroll(View.FOCUS_DOWN);
                    }
                });
            }
        }).start();
    }

    public static void hideSoftKeyboard(Fragment activity) {
        InputMethodManager inputMethodManager =
                (InputMethodManager) activity.getActivity().getSystemService(
                        Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(
                activity.getActivity().getCurrentFocus().getWindowToken(), 0);
    }

    private void init() {
        replayname = view.findViewById(R.id.replayname);
        chatscroll = view.findViewById(R.id.chatscroll);
        replaylayout = view.findViewById(R.id.replaylayout);
        llComment = view.findViewById(R.id.llComment);
        chat_text = view.findViewById(R.id.chat_text);
        chat_send_text = view.findViewById(R.id.chat_send_text);
        commentrecycler = view.findViewById(R.id.commentrecycler);
        linearLayoutManager = new LinearLayoutManager(getContext());
        commentrecycler.setLayoutManager(linearLayoutManager);

        blogDetailAdapter = new BlogDetailAdapter(chatModelArrayList);
        commentrecycler.addOnItemTouchListener(new RecyclerTouchListener(getContext(), commentrecycler, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                view.findViewById(R.id.reply).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Log.d("+++else", "++if ");

                        comment_id = chatModelArrayList.get(position).getComment_id();
                        if (comment_id.equalsIgnoreCase("")) {
                            comment_id = c_id;
                            Log.d("++++cmtId", "++" + comment_id);
                        }
                        Log.d("++++cmtId", "++1 " + comment_id);
                        replaylayout.setVisibility(View.VISIBLE);
                        replayname.setText("Replying to_" + chatModelArrayList.get(position).getVar_name());
                        pos = position;
                        //chat_text.setText(Html.fromHtml("<b>"+chatModelArrayList.get(pos).getVar_name()+"</b>" + " "));
                        final int position = chat_text.length();
                        Editable etext = chat_text.getText();
                        Selection.setSelection(etext, position);
                        comment_type = "R";

                    }
                });
            }

            @Override
            public void onLongClick(View view, final int position) {
            }

        }));
        commentrecycler.setAdapter(blogDetailAdapter);
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            blogId = bundle.getString("blogid");
        }
    }

    private void CallAddComment() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.addComment, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++addcomment", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {
                            comment_id = jsonObject.getString("comment_id");
                            c_id = comment_id;
                            new MakeToast(jsonObject.getString("message"));
                        } else {
                            new MakeToast(jsonObject.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "get_blog_comment_list");
                params.put("blog_id", blogId);
                params.put("comment_id", comment_id);
                params.put("user_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id, Constant.notAvailable));
                params.put("message", msg);
                Log.d("++++addcomment", "+++params " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void CallCommentList() {
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.commentList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++addcomment", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {
                            chatModelArrayList.clear();
                            JSONArray jsonArray = jsonObject.getJSONArray("comment_list");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                ArrayList<ChatReplyModel> chatReplyModelsArray = new ArrayList<>();
                                chatReplyModelsArray.clear();
                                JSONObject object = jsonArray.getJSONObject(i);
                                ChatModel chatModel = new ChatModel();
                                chatModel.setComment_id(object.getString("comment_id"));
                                chatModel.setBlog_id(object.getString("blog_id"));
                                chatModel.setUser_id(object.getString("user_id"));
                                chatModel.setVar_name(object.getString("var_name"));
                                chatModel.setVar_massage(object.getString("var_massage"));
                                chatModel.setDate(object.getString("date"));
                                chatModel.setTime(object.getString("time"));
                                // chatModel.setVar_image(object.getString("var_image"));

                                comment_id = object.getString("comment_id");
                                //  msg = object.getString("var_massage");
                                JSONArray array = object.optJSONArray("comment_reply");
                                Log.d("++++addcomment", "+++json ");

                                if (array.length() > 0) {
                                    for (int j = 0; j < array.length(); j++) {
                                        JSONObject object1 = array.getJSONObject(j);
                                        ChatReplyModel chatReplyModel = new ChatReplyModel();
                                        chatReplyModel.setReply_id(object1.getString("reply_id"));
                                        chatReplyModel.setBlog_id(object1.getString("blog_id"));
                                        chatReplyModel.setUser_id(object1.getString("user_id"));
                                        chatReplyModel.setVar_name(object1.getString("var_name"));

                                        chatReplyModel.setVar_massage(object1.getString("var_massage"));
                                        chatReplyModel.setNew_name(object1.getString("new_name"));
                                        chatReplyModel.setNew_message(object1.getString("new_message"));
                                        chatReplyModel.setDate(object1.getString("date"));
                                        chatReplyModel.setTime(object1.getString("time"));
                                        //  chatReplyModel.setVar_image(object1.getString("var_image"));

                                        chatReplyModelsArray.add(chatReplyModel);
                                    }
                                }
                                chatModel.setChatReplyModelArrayList(chatReplyModelsArray);
                                chatModelArrayList.add(chatModel);
                            }
                            blogDetailAdapter = new BlogDetailAdapter(chatModelArrayList);
                            commentrecycler.setAdapter(blogDetailAdapter);

                            if (chatModelArrayList.size() != 0) {
                                Log.d("+++else", "++if ");
                                llComment.setVisibility(View.VISIBLE);
                            } else {
                                Log.d("+++else", "++else ");
                                llComment.setVisibility(View.GONE);
                            }
                        } else {
                            new MakeToast(jsonObject.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "get_blog_comment_list");
                params.put("blog_id", blogId);
                Log.d("++++addcomment", "+++params " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }


}
