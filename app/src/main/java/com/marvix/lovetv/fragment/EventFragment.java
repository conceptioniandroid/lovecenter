package com.marvix.lovetv.fragment;

import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.bumptech.glide.Priority;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.makeramen.roundedimageview.RoundedImageView;
import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.model.EventModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.DBOpenHelper;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.TextviewBold;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.relex.circleindicator.CircleIndicator;

public class EventFragment extends Fragment {
    View view;
    ViewPager viewPager;
    Toolbar toolbar;
    ArrayList<EventModel> eventModelsArray = new ArrayList<>();
    ProgressBar progress;
    TextviewRegular tvrNodata;
    String title = "", blogId = "", place = "", address = "";
    long startDate = 0, eventID, startTime, endDate = 0;
    int page = 1, CurrentViewpagerPosition = 0, Total_Pages;
    SlidePageAdapter slidePageAdapter;
    String TAG = "ListData";
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    Context context;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_event, container, false);
        context = container.getContext();
        init();
        clicks();
        return view;
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void init() {
        tvrNodata = view.findViewById(R.id.tvrNodata);
        progress = view.findViewById(R.id.progress);
        viewPager = view.findViewById(R.id.pager);
        toolbar = view.findViewById(R.id.toolbar);

        slidePageAdapter = new SlidePageAdapter();
        if (viewPager != null) {
            viewPager.setAdapter(slidePageAdapter);
        }

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            blogId = bundle.getString("eventId");
        }


        if (isNetConnectionAvailable())
            callEvent(page);
        else {

            String data = "";
            dbOpenHelper = new DBOpenHelper(context);
            sqLiteDatabase = dbOpenHelper.getReadableDatabase();
            data = dbOpenHelper.getEventList();

            if (!data.equalsIgnoreCase("")) {
                Gson gson = new Gson();
                eventModelsArray = gson.fromJson(data, new TypeToken<List<EventModel>>() {
                }.getType());
                slidePageAdapter.notifyDataSetChanged();
            } else {
                Toast.makeText(context, "No offline data available", Toast.LENGTH_SHORT).show();
            }
            progress.setVisibility(View.GONE);
        }

        viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                Log.d("TAG", "onPageSelected: " + i + "++++" + eventModelsArray.size());
                CurrentViewpagerPosition = i;
                if (CurrentViewpagerPosition == eventModelsArray.size() - 3) {
                    if (Total_Pages == page) {
                        Log.d("TAG{", "onPageSelected: 123" + Total_Pages + page);
//                        page = 1;
                    } else {
                        page = page + 1;
                        if (isNetConnectionAvailable())
                            callEvent(page);
                    }

                }
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });
    }

    class SlidePageAdapter extends PagerAdapter {
        private LayoutInflater layoutInflater;

        @Override
        public int getCount() {
            return eventModelsArray.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public Object instantiateItem(ViewGroup container, final int i) {
            layoutInflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View view = layoutInflater.inflate(R.layout.row_item_event_slider, container, false);
            RoundedImageView imageView1 = view.findViewById(R.id.imageView1);
            TextviewBold tvbTitle = view.findViewById(R.id.tvbTitle);
            TextviewMedium tvmDate = view.findViewById(R.id.tvmDate);
            TextviewMedium tvmTime = view.findViewById(R.id.tvmTime);
            TextviewMedium tvmAddress = view.findViewById(R.id.tvmAddress);
            TextviewRegular tvrSaveEvent = view.findViewById(R.id.tvrSaveEvent);
            container.addView(view);
            RequestOptions options = new RequestOptions()
                    .centerCrop()
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .priority(Priority.HIGH);
            Glide.with(getActivity()).load(eventModelsArray.get(i).getVar_image()).apply(options).into(imageView1);
            tvbTitle.setText(eventModelsArray.get(i).getVar_title());
            if (eventModelsArray.get(i).getDate().equalsIgnoreCase("") || eventModelsArray.get(i).getDate().equals(eventModelsArray.get(i).getEnd_date())) {
                tvmDate.setText(eventModelsArray.get(i).getDate());
            } else {
                tvmDate.setText(eventModelsArray.get(i).getDate() + " to " + eventModelsArray.get(i).getEnd_date());
            }
            tvmTime.setText(eventModelsArray.get(i).getTime());
            tvmAddress.setText(eventModelsArray.get(i).getAddress());

//            String givenDateString = "Wed Sep 10 16:08:28 GMT+05:30 2018";


//            String givenDateString = eventModelsArray.get(i).getEnd_date();
//            SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy");
//
//            try {
//                Date mDate = sdf.parse(givenDateString);
//                timeInMilliseconds = mDate.getTime();
//                System.out.println("Date in milli :: " + timeInMilliseconds);
//            } catch (ParseException e) {
//                e.printStackTrace();
//            }


            tvrSaveEvent.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String EndDate = eventModelsArray.get(i).getEnd_date();
                    String StartDate = eventModelsArray.get(i).getDate();
                    long timeInMilliseconds = 0;
                    long timeInMillisecondsfirst = 0;
                    try {
                        Date date1 = new SimpleDateFormat("MMM dd,yyyy").parse(EndDate);
                        Date date2 = new SimpleDateFormat("MMM dd,yyyy").parse(StartDate);

                        Date tomorrow = new Date(date1.getTime() + (1000 * 60 * 60 * 24));

                        timeInMilliseconds = tomorrow.getTime();
                        timeInMillisecondsfirst = date2.getTime();
                        Log.d("TAG", "instantiateItem: " + tomorrow);
                        System.out.println("Date in milli :: " + timeInMilliseconds);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    final long finalTimeInMilliseconds = timeInMilliseconds;
                    final long finalTimeInMillisecondsfirst = timeInMillisecondsfirst;
                    Intent intent = new Intent(Intent.ACTION_EDIT);
                    intent.setType("vnd.android.cursor.item/event");
//                    intent.putExtra("beginTime", eventModelsArray.get(i).getTime());
                    intent.putExtra("title", eventModelsArray.get(i).getVar_title());
                    intent.putExtra("address", eventModelsArray.get(i).getAddress());
                    intent.putExtra("allDay", true);
                    intent.putExtra("rule", "FREQ=YEARLY");
                    Log.d("TAG", "onClick: " + eventModelsArray.get(i).getEnd_date());
                    intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, finalTimeInMilliseconds);
                    intent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, finalTimeInMillisecondsfirst);
                    startActivity(intent);
                }
            });
            return view;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }


    private void callEvent(final int Page) {
        if (Page == 1) {
            progress.setVisibility(View.VISIBLE);
        }

        tvrNodata.setVisibility(View.GONE);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.event, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++reg", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {

                            dbOpenHelper = new DBOpenHelper(context);
                            sqLiteDatabase = dbOpenHelper.getWritableDatabase();
                            dbOpenHelper.deleteEventTable();

                            if (Page == 1) {
                                eventModelsArray.clear();
                            }
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.GONE);
                            Total_Pages = jsonObject.getInt("total_pages");
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                EventModel eventModel = new EventModel();
                                JSONObject object = jsonArray.getJSONObject(i);
                                eventModel.setInt_glcode(object.optString("int_glcode"));
                                eventModel.setVar_title(object.optString("var_title"));
                                eventModel.setVar_image(object.optString("var_image"));
                                eventModel.setDate(object.optString("date"));
                                eventModel.setTime(object.optString("time"));
                                eventModel.setAddress(object.optString("address"));
                                eventModel.setEnd_date(object.optString("end_date"));
                                eventModelsArray.add(eventModel);
                            }

                            slidePageAdapter.notifyDataSetChanged();

                            if (eventModelsArray.size() > 0) {
                                Gson gson = new Gson();
                                String data = gson.toJson(eventModelsArray);
                                long id = dbOpenHelper.addEvent(data);
                            }

                            CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
                            indicator.setViewPager(viewPager);
                            if (blogId != null) {
                                Log.d("++++blogid", "++if " + blogId + "++++ ");

                                for (int i = 0; i < eventModelsArray.size(); i++) {
                                    Log.d("++++blogid", "++for " + blogId + "++++ " + eventModelsArray.get(i).getInt_glcode());

                                    if (eventModelsArray.get(i).getInt_glcode().equalsIgnoreCase(blogId)) {
                                        Log.d("++++blogid", "++ " + blogId + "++++ " + eventModelsArray.get(i).getInt_glcode());
                                        viewPager.setCurrentItem(i);
                                    }
                                }
                            }
                        } else {
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.VISIBLE);
                            new MakeToast(jsonObject.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                        tvrNodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "event_list");
                params.put("page", "" + Page);
                Log.d("++++reg", "+++params " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) LoveCenter.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public void onDestroy() {
        Log.d(TAG, "onDestroy: ");
        FragmentManager fm = getActivity().getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); ++i) {
            fm.popBackStack();
        }
        Fragment fragment = new HomeFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
        super.onDestroy();
    }
}
