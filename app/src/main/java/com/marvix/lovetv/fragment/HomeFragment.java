package com.marvix.lovetv.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.utils.Constant;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    LinearLayout llBlog, llVideo, llAudio, llevent;
    LinearLayout lin1, lin2, lin3, lin4;
    ImageView iv1, iv2, iv3, iv4;
    View view;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void init() {

        DisplayMetrics displayMetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int height = displayMetrics.heightPixels;
        int width = displayMetrics.widthPixels;

        llevent = view.findViewById(R.id.llevent);
        llBlog = view.findViewById(R.id.llBlog);
        llVideo = view.findViewById(R.id.llVideo);
        llAudio = view.findViewById(R.id.llAudio);

        lin1 = view.findViewById(R.id.lin1);
        lin2 = view.findViewById(R.id.lin2);
        lin3 = view.findViewById(R.id.lin3);
        lin4 = view.findViewById(R.id.lin4);

        iv1 = view.findViewById(R.id.iv1);
        iv2 = view.findViewById(R.id.iv2);
        iv3 = view.findViewById(R.id.iv3);
        iv4 = view.findViewById(R.id.iv4);

        lin1.getLayoutParams().width = (int) (width * 0.22);
        lin1.getLayoutParams().height = (int) (width * 0.22);
        lin1.requestLayout();

        lin2.getLayoutParams().width = (int) (width * 0.22);
        lin2.getLayoutParams().height = (int) (width * 0.22);
        lin2.requestLayout();

        lin3.getLayoutParams().width = (int) (width * 0.22);
        lin3.getLayoutParams().height = (int) (width * 0.22);
        lin3.requestLayout();

        lin4.getLayoutParams().width = (int) (width * 0.22);
        lin4.getLayoutParams().height = (int) (width * 0.22);
        lin4.requestLayout();

        iv1.getLayoutParams().width = (int) (width * 0.08);
        iv1.getLayoutParams().height = (int) (width * 0.08);
        iv1.requestLayout();

        iv2.getLayoutParams().width = (int) (width * 0.08);
        iv2.getLayoutParams().height = (int) (width * 0.08);
        iv2.requestLayout();

        iv3.getLayoutParams().width = (int) (width * 0.08);
        iv3.getLayoutParams().height = (int) (width * 0.08);
        iv3.requestLayout();

        iv4.getLayoutParams().width = (int) (width * 0.08);
        iv4.getLayoutParams().height = (int) (width * 0.08);
        iv4.requestLayout();
        
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {
        llBlog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new BlogFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content, fragment);
                ft.addToBackStack("");
                ft.commit();
            }
        });
        llVideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new VideoListFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content, fragment);
                ft.addToBackStack("");
                ft.commit();
            }
        });
        llAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment;
                if (isNetConnectionAvailable())
                    fragment = new AudioFragment();
                else
                    fragment = new OfflineaudioFragment();

                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content, fragment);
                ft.addToBackStack("");
                ft.commit();
            }
        });
        llevent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new EventFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                ft.replace(R.id.content, fragment);
                ft.addToBackStack("");
                ft.commit();
            }
        });
    }

    private boolean isNetConnectionAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) LoveCenter.getContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
