package com.marvix.lovetv.fragment;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marvix.lovetv.R;
import com.marvix.lovetv.adapter.OfflineAdapter;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.DBOpenHelper;
import com.marvix.lovetv.utils.ItemOffsetDecoration;

import java.util.ArrayList;

public class MydownloadsFragment extends Fragment {
    View view;
    Context context;
    RecyclerView rec;
    GridLayoutManager gridLayoutManager;
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<TopAudioModel> recentAudioModelArrayList = new ArrayList<>();
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        view = inflater.inflate(R.layout.fragment_downloads, container, false);
        rec = view.findViewById(R.id.rec);
        toolbar = view.findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        rec.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset);
        rec.addItemDecoration(itemDecoration);

        dbOpenHelper = new DBOpenHelper(context);
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        recentAudioModelArrayList = dbOpenHelper.getAudioList();

        OfflineAdapter topAudioAdapter = new OfflineAdapter(getActivity(), recentAudioModelArrayList);
        rec.setAdapter(topAudioAdapter);

        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        Fragment fragment = new AudioFragment();
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction ft = fragmentManager.beginTransaction();
        ft.replace(R.id.content, fragment);
        ft.commit();
    }
}
