package com.marvix.lovetv.fragment;

import android.content.Context;
import android.content.ContextWrapper;
import android.database.sqlite.SQLiteDatabase;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.SplashActivity;
import com.marvix.lovetv.adapter.OfflineAdapter;
import com.marvix.lovetv.adapter.TopAudioAdapter;
import com.marvix.lovetv.model.RecentAudioModel;
import com.marvix.lovetv.model.TopAudioModel;
import com.marvix.lovetv.utils.DBOpenHelper;
import com.marvix.lovetv.utils.ItemOffsetDecoration;

import java.io.IOException;
import java.util.ArrayList;

import dm.audiostreamer.MediaMetaData;

public class OfflineaudioFragment extends Fragment {
    View view;
    Context context;
    RecyclerView rec;
    GridLayoutManager gridLayoutManager;
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;
    ArrayList<TopAudioModel> recentAudioModelArrayList = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        context = container.getContext();
        view = inflater.inflate(R.layout.fragment_offline, container, false);
        rec = view.findViewById(R.id.rec);

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        gridLayoutManager = new GridLayoutManager(getActivity(), 4);
        rec.setLayoutManager(gridLayoutManager);
        ItemOffsetDecoration itemDecoration = new ItemOffsetDecoration(getContext(), R.dimen.item_offset);
        rec.addItemDecoration(itemDecoration);

        dbOpenHelper = new DBOpenHelper(context);
        sqLiteDatabase = dbOpenHelper.getReadableDatabase();

        recentAudioModelArrayList = dbOpenHelper.getAudioList();

        OfflineAdapter topAudioAdapter = new OfflineAdapter(getActivity(), recentAudioModelArrayList);
        rec.setAdapter(topAudioAdapter);

        return view;
    }

}
