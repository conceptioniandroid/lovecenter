package com.marvix.lovetv.fragment;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;
import com.marvix.lovetv.activity.LoginActivity;
import com.marvix.lovetv.adapter.MinistryAdapter;
import com.marvix.lovetv.adapter.VideoAdapter;
import com.marvix.lovetv.model.MinistryModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

public class OurChurchFragment extends Fragment {
    RecyclerView rvMinistries;
    LinearLayoutManager linearLayoutManager;
    View view;
    MinistryAdapter ministryAdapter;
    TextviewRegular tvrDesc,tvrNodata,tvrPhone;
    TextviewMedium tvmTitle;
    LinearLayout llChurch,llBottom;
    ProgressBar progress;
    ArrayList<MinistryModel> ministryModelsArray = new ArrayList<>();
    String title="", desc="",phone="";
    Toolbar toolbar;
    int img[]={R.drawable.a,R.drawable.b,R.drawable.c,R.drawable.d,R.drawable.e,R.drawable.f,R.drawable.g,R.drawable.h,R.drawable.i,R.drawable.j,R.drawable.k};
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_our_church, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        tvrPhone = view.findViewById(R.id.tvrPhone);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        progress = view.findViewById(R.id.progress);
        llChurch = view.findViewById(R.id.llChurch);
        llBottom = view.findViewById(R.id.llBottom);
        rvMinistries = view.findViewById(R.id.rvMinistries);
        tvmTitle = view.findViewById(R.id.tvmTitle);
        tvrDesc = view.findViewById(R.id.tvrDesc);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvMinistries.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));

        for (int i = 0; i < img.length; i++) {
            MinistryModel  ministryModel=new MinistryModel();
            ministryModel.setVar_image(img[i]);
            ministryModelsArray.add(ministryModel);
        }
        ministryAdapter = new MinistryAdapter(ministryModelsArray);
        rvMinistries.setAdapter(ministryAdapter);
    }

//    private void callChurch() {
//        progress.setVisibility(View.VISIBLE);
//        llChurch.setVisibility(View.GONE);
//        llBottom.setVisibility(View.GONE);
//        tvrNodata.setVisibility(View.GONE);
//        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
//        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.cherech, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//                if (response != null) {
//                    try {
//                        JSONObject jsonObject = new JSONObject(response);
//                        Log.d("++++reg", "+++json " + jsonObject);
//                        if (jsonObject.optInt("success") == 1) {
//                            ministryModelsArray.clear();
//                            progress.setVisibility(View.GONE);
//                            llChurch.setVisibility(View.VISIBLE);
//                            llBottom.setVisibility(View.VISIBLE);
//                            tvrNodata.setVisibility(View.GONE);
//                            JSONObject object = jsonObject.getJSONObject("data");
//                            title= object.optString("var_title");
//                            phone=object.optString("var_mobileno");
//                            desc=object.optString("var_description");
//                            Log.d("++++reg","+++title"+title);
//                            JSONArray jsonArray = jsonObject.getJSONArray("our_client");
//                            for (int i = 0; i < jsonArray.length(); i++) {
//                                MinistryModel ministryModel = new MinistryModel();
//                                JSONObject object1 = jsonArray.getJSONObject(i);
//                                ministryModel.setVar_image(object1.optString("var_image"));
//                                ministryModelsArray.add(ministryModel);
//                            }
//                            tvmTitle.setText(title);
//                            tvrDesc.setText(desc);
//                            tvrPhone.setText("Tel : "+phone);
//                            ministryAdapter = new MinistryAdapter(ministryModelsArray);
//                            rvMinistries.setAdapter(ministryAdapter);
//                        } else {
//                            progress.setVisibility(View.GONE);
//                            llChurch.setVisibility(View.GONE);
//                            llBottom.setVisibility(View.GONE);
//                            tvrNodata.setVisibility(View.VISIBLE);
//                            new MakeToast(jsonObject.optString("message"));
//                        }
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        new MakeToast(e.getMessage());
//                        progress.setVisibility(View.GONE);
//                        llChurch.setVisibility(View.GONE);
//                        llBottom.setVisibility(View.GONE);
//                        tvrNodata.setVisibility(View.VISIBLE);
//                    }
//                }
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() {
//                Map<String, String> params = new HashMap<String, String>();
//                params.put("API", "cherech");
//                params.put("user_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id,Constant.notAvailable));
//
//                Log.d("++++reg", "+++params " + params);
//
//                return params;
//            }
//        };
//        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//        stringRequest.setShouldCache(false);
//        requestQueue.add(stringRequest);
//    }

}
