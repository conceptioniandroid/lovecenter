package com.marvix.lovetv.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.Toolbar;

import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.LoginActivity;
import com.marvix.lovetv.dialogue.ChangePassword;
import com.marvix.lovetv.utils.SharedPrefs;

public class SettingFragment extends Fragment {

    View view;
    RelativeLayout logoutrl, changepassrl;
    Toolbar toolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.setting, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {
        logoutrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showLogoutAlert();
            }
        });

        changepassrl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new ChangePassword(getActivity()) {

                }.ShowChangeDialogue();
            }
        });
    }

    private void init() {
        logoutrl = view.findViewById(R.id.logoutrl);
        changepassrl = view.findViewById(R.id.changepassrl);

        view.findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });
    }

    private void showLogoutAlert() {
        new AlertDialog.Builder(getActivity())
                .setTitle("Logout?")
                .setMessage("Are you sure want to logout?")
                .setCancelable(true)

                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface arg0, int arg1) {
                        SharedPrefs.getSharedPref().edit().remove(SharedPrefs.userSharedPrefData.user_id).commit();
                        startActivity(new Intent(getActivity(), LoginActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                    }
                })
                .create().show();
    }

}
