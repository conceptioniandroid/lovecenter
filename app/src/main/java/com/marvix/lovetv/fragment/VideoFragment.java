package com.marvix.lovetv.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.model.PlaylistListResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchProviderException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.adapter.Adaptercopy;
import com.marvix.lovetv.adapter.RelatedVideoAdapter;
import com.marvix.lovetv.adapter.VideoRecyclerAdapter;
import com.marvix.lovetv.listener.OnLoadMoreListener;
import com.marvix.lovetv.model.PlaylistItem;
import com.marvix.lovetv.model.PlaylistResponse;
import com.marvix.lovetv.model.RelatedVideoModel;
import com.marvix.lovetv.model.YouTubeModel;
import com.marvix.lovetv.utils.Config;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.RecyclerTouchListener;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewMedium;
import com.marvix.lovetv.utils.TextviewRegular;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;

import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static com.google.android.youtube.player.YouTubePlayer.FULLSCREEN_FLAG_CONTROL_ORIENTATION;


public class VideoFragment extends Fragment {
    View view;
    RecyclerView rvRelatedVideo;
    RelatedVideoAdapter relatedVideoAdapter;
    LinearLayoutManager linearLayoutManager;
    TextviewMedium tvmTitle;
    TextviewRegular tvrDescp, tvrNodata;
    ArrayList<RelatedVideoModel> relatedVideoModelsArray = new ArrayList<>();
    ProgressBar progress;
    String image = "", title = "", desc = "", link = "";
    String videoId = "",videoDes="",videoName="";
    Toolbar toolbar;
    ViewPager pager;
    CircleIndicator indicator;
    private int totalResults = 0;
    private String nextPageToken;
    ArrayList<PlaylistItem> playlistItems = new ArrayList<>();
    ArrayList<PlaylistItem> item = new ArrayList<>();
    VideoRecyclerAdapter videoRecyclerAdapter;
    RecyclerView rvVideo;
    YouTubePlayerSupportFragment youTubePlayerFragment;
    boolean youTubePlayerIsFullScreen;
    private boolean mAutoRotation = false;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_video2, container, false);
        init();
        clicks();

        return view;
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onClick(View v) {
                getFragmentManager().popBackStack();
            }
        });
    }

    private void init() {
        rvVideo = view.findViewById(R.id.rvVideo);
        indicator = view.findViewById(R.id.indicator);
        pager = view.findViewById(R.id.pager);
        toolbar = view.findViewById(R.id.toolbar);
        progress = view.findViewById(R.id.progress);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        tvmTitle = view.findViewById(R.id.tvmTitle);
        tvrDescp = view.findViewById(R.id.tvrDescp);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            videoId = bundle.getString("videoId");
            videoName = bundle.getString("videoName");
            videoDes = bundle.getString("videoDes");
        }
        tvmTitle.setText(videoName);
        tvrDescp.setText(videoDes);

        youTubePlayerFragment = YouTubePlayerSupportFragment.newInstance();

        FragmentTransaction transaction = getChildFragmentManager().beginTransaction();
        transaction.add(R.id.frameLayout, youTubePlayerFragment).commit();

        youTubePlayerFragment.initialize(Config.YOUTUBE_API_KEY_NEW, new YouTubePlayer.OnInitializedListener() {

            @Override
            public void onInitializationSuccess(YouTubePlayer.Provider provider, final YouTubePlayer player, boolean wasRestored) {
                if (!wasRestored) {
                    player.setPlayerStyle(YouTubePlayer.PlayerStyle.DEFAULT);
                    player.setFullscreenControlFlags(2);
                    player.setShowFullscreenButton(true);


                    player.setOnFullscreenListener(new YouTubePlayer.OnFullscreenListener() {
                        @Override
                        public void onFullscreen(boolean b) {
                            if(b) {
                                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//                                PreferenceUtils.setDisplayMedia(b);
                            }
                            else {
                                getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//                                PreferenceUtils.setDisplayMedia(b);
                            }
                        }
                    });

                    player.loadVideo(videoId);
                    player.play();
                }
            }

            @Override
            public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult error) {
                // YouTube error
                String errorMessage = error.toString();
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_LONG).show();
                Log.d("errorMessage:", errorMessage);
            }
        });

    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d("TAG", "onFullscreen:pause " );

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);


    }
}
