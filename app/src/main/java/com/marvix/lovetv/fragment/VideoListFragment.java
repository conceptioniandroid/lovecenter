package com.marvix.lovetv.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.marvix.lovetv.R;
import com.marvix.lovetv.adapter.MinistryAdapter;
import com.marvix.lovetv.adapter.VideoAdapter;
import com.marvix.lovetv.model.MinistryModel;
import com.marvix.lovetv.model.VideoListModel;
import com.marvix.lovetv.model.YouTubeModel;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.RecyclerTouchListener;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewRegular;

public class VideoListFragment extends Fragment {
    RecyclerView rvVideo;
    LinearLayoutManager linearLayoutManager;
    View view;
    VideoAdapter videoAdapter;
    ArrayList<VideoListModel> videoListModels = new ArrayList<>();
    TextviewRegular tvrNodata;
    ProgressBar progress;
    Toolbar toolbar;
    ArrayList<YouTubeModel> youTubeModelsarray = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_video, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });
        rvVideo.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvVideo, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Fragment fragment = new VideoPlayListFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("videoId", youTubeModelsarray.get(position).getId());
                fragment.setArguments(bundle);
                ft.addToBackStack("");
                ft.replace(R.id.content, fragment);
                ft.commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        rvVideo = view.findViewById(R.id.rvVideo);
        progress = view.findViewById(R.id.progress);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvVideo.setLayoutManager(linearLayoutManager);
        //  callvideoList();
        getplaylistid();
    }

    public void getplaylistid() {

        String url = "https://www.googleapis.com/youtube/v3/playlists?part=snippet&channelId=UCw5QSQSsfXK8xAPzm5LHSgA&key=AIzaSyAyfCGn2k6gnNjhrdUF5fRmRLbtsL2cICI";
        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new com.android.volley.Response.Listener<String>() {
            @Override
            public void onResponse(@Nullable String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        youTubeModelsarray.clear();
                        JSONArray jsonArray = jsonObject.getJSONArray("items");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            YouTubeModel youTubeModel = new YouTubeModel();
                            JSONObject object = jsonArray.getJSONObject(i);
                            youTubeModel.setId(object.getString("id"));
                            JSONObject object1 = object.getJSONObject("snippet");
                            youTubeModel.setTitle(object1.optString("title"));
                            youTubeModel.setDesc(object1.optString("description"));
                            JSONObject object2 = object1.optJSONObject("thumbnails");
                            JSONObject object3 = new JSONObject();
                            if (object2 != null){
                                object3 = object2.optJSONObject("standard");
                            }if (object3 != null){
                                youTubeModel.setThumb(object3.optString("url"));
                            }
                            youTubeModelsarray.add(youTubeModel);
                        }
                        videoAdapter = new VideoAdapter(youTubeModelsarray);
                        rvVideo.setAdapter(videoAdapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }
        },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        //  new MakeToast("Could not able to login due to slow internet connectivity. Please try after some time");
                    }
                }) {

        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(60000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        requestQueue.add(stringRequest);
    }
}
