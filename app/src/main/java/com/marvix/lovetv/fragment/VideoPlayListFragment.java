package com.marvix.lovetv.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.marvix.lovetv.LoveCenter;
import com.marvix.lovetv.R;
import com.marvix.lovetv.adapter.VideoPlaylistAdapter;
import com.marvix.lovetv.model.PlaylistItem;
import com.marvix.lovetv.model.PlaylistResponse;
import com.marvix.lovetv.model.VideoListModel;
import com.marvix.lovetv.model.YouTubeModel;
import com.marvix.lovetv.utils.Config;
import com.marvix.lovetv.utils.Constant;
import com.marvix.lovetv.utils.MakeToast;
import com.marvix.lovetv.utils.RecyclerTouchListener;
import com.marvix.lovetv.utils.SharedPrefs;
import com.marvix.lovetv.utils.TextviewRegular;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;

public class VideoPlayListFragment extends Fragment {
    RecyclerView rvVideo;
    LinearLayoutManager linearLayoutManager;
    View view;
    VideoPlaylistAdapter videoAdapter;
    ArrayList<VideoListModel> videoListModels = new ArrayList<>();
    TextviewRegular tvrNodata;
    ProgressBar progress;
    Toolbar toolbar;
    ArrayList<YouTubeModel> youTubeModelsarray = new ArrayList<>();
    String videoId = "";
    private int totalResults = 0;
    private String nextPageToken;
    ArrayList<PlaylistItem> playlistItems = new ArrayList<>();
    VideoPlaylistAdapter adaptercopy;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_video_playlist, container, false);
        init();
        clicks();
        return view;
    }

    private void clicks() {

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                assert getFragmentManager() != null;
                getFragmentManager().popBackStack();
            }
        });

        rvVideo.addOnItemTouchListener(new RecyclerTouchListener(getActivity(), rvVideo, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                Fragment fragment = new VideoFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction ft = fragmentManager.beginTransaction();
                Bundle bundle = new Bundle();
                bundle.putString("videoId", playlistItems.get(position).getItemSnippet().getSnippetResourceId().getVideoId());
                bundle.putString("videoName", playlistItems.get(position).getItemSnippet().getTitle());
                bundle.putString("videoDes", playlistItems.get(position).getItemSnippet().getDescription());
                fragment.setArguments(bundle);
                ft.addToBackStack("");
                ft.replace(R.id.content, fragment);
                ft.commit();

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }

    private void init() {
        toolbar = view.findViewById(R.id.toolbar);
        rvVideo = view.findViewById(R.id.rvVideo);
        progress = view.findViewById(R.id.progress);
        tvrNodata = view.findViewById(R.id.tvrNodata);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        rvVideo.setLayoutManager(linearLayoutManager);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            videoId = bundle.getString("videoId");
        }
        getAnyPlaylist();
    }

    private void callvideoList() {
        progress.setVisibility(View.VISIBLE);
        tvrNodata.setVisibility(View.GONE);
        RequestQueue requestQueue = Volley.newRequestQueue(getActivity());
        StringRequest stringRequest = new StringRequest(Request.Method.POST, Constant.videoList, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        Log.d("++++reg", "+++json " + jsonObject);
                        if (jsonObject.optInt("success") == 1) {
                            videoListModels.clear();
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.GONE);
                            JSONArray jsonArray = jsonObject.getJSONArray("data");
                            for (int i = 0; i < jsonArray.length(); i++) {
                                VideoListModel videoListModel = new VideoListModel();
                                JSONObject object = jsonArray.getJSONObject(i);
                                videoListModel.setInt_glcode(object.optString("int_glcode"));
                                videoListModel.setVar_image(object.optString("var_image"));
                                videoListModel.setVar_link(object.optString("var_link"));
                                videoListModel.setVar_title(object.optString("var_title"));
                                videoListModel.setVar_description(object.optString("var_description"));
                                videoListModels.add(videoListModel);
                            }

                        } else {
                            progress.setVisibility(View.GONE);
                            tvrNodata.setVisibility(View.VISIBLE);
                            new MakeToast(jsonObject.optString("message"));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        new MakeToast(e.getMessage());
                        progress.setVisibility(View.GONE);
                        tvrNodata.setVisibility(View.VISIBLE);
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                new MakeToast("Could not able to load due to slow internet connectivity. Please try after some time");
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("API", "video_list");
                params.put("user_id", SharedPrefs.getSharedPref().getString(SharedPrefs.userSharedPrefData.user_id, Constant.notAvailable));

                Log.d("++++reg", "+++params " + params);

                return params;
            }
        };
        stringRequest.setRetryPolicy(new DefaultRetryPolicy(90000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        stringRequest.setShouldCache(false);
        requestQueue.add(stringRequest);
    }

    private void getAnyPlaylist() {
        progress.setVisibility(View.VISIBLE);
        Map<String, String> firstFilters = new HashMap<>();
        firstFilters.put("part", "snippet");
        firstFilters.put("playlistId", videoId);
        firstFilters.put("key", Config.YOUTUBE_API_KEY_NEW);


        Call<PlaylistResponse> getAnyPlaylist = LoveCenter.getYoutubeClient().getService().getAnyPlaylist(firstFilters);

        Log.d("TAG", "getAnyPlaylist2: " + firstFilters);

        getAnyPlaylist.enqueue(new Callback<PlaylistResponse>() {

            @Override
            public void onResponse(retrofit2.Response<PlaylistResponse> playlistResponse) {
                Log.d("TAG", "if: ");
                progress.setVisibility(View.GONE);

                if (playlistResponse.isSuccess() && playlistResponse.body().getPlaylistItems().size() != 0) {

                    totalResults = playlistResponse.body().getResponsePageInfo().getTotalResults();
                    nextPageToken = playlistResponse.body().getNextPageToken();
                    Log.d("TAG", "onResponse:nextPageToken " + nextPageToken);

                    playlistItems = playlistResponse.body().getPlaylistItems();
                    Log.d("TAG", "onResponse:size " + playlistItems.size());

//                    videoRecyclerAdapter = new VideoRecyclerAdapter(rvVideo,playlistItems,new OnLoadMoreListener() {
//
//                        @Override
//                        public void onLoadMore() {
//                            //loadAnyPlaylistMore();
//                        }
//                    },getContext(),VideoFragment.this);
//                    rvVideo.setAdapter(videoRecyclerAdapter);
                    adaptercopy = new VideoPlaylistAdapter(playlistItems);
                    rvVideo.setAdapter(adaptercopy);
                    rvVideo.setVisibility(View.VISIBLE);
//                    videoRecyclerAdapter = new VideoRecyclerAdapter(rvVideo,playlistItems, new OnLoadMoreListener() {
//                        @Override
//                        public void onLoadMore() {
//
//                        }
//                    },getContext());
//                    rvVideo.setAdapter(videoRecyclerAdapter);
//                     slidePageAdapter = new SlidePageAdapter();
//                    if (pager != null) {
//                        pager.setAdapter(slidePageAdapter);
//                        LayoutInflater inflater = getActivity().getLayoutInflater();
//                        FrameLayout v0 = (FrameLayout) inflater.inflate (R.layout.row_item_video_slider, null);
//                      //  slidePageAdapter.addView (v0, 0);
//                        slidePageAdapter.notifyDataSetChanged();
//                    }
//                    CircleIndicator indicator = (CircleIndicator) view.findViewById(R.id.indicator);
//                    indicator.setViewPager(pager);
                } else {
                    Log.d("TAG", "erroe: " + playlistItems.size());

                }
            }

            @Override
            public void onFailure(Throwable t) {
                Log.d("TAG", "else: " + t.getMessage());
                progress.setVisibility(View.GONE);
            }
        });
    }

}
