package com.marvix.lovetv.listener;

public interface OnLoadMoreListener {

    void onLoadMore();
}
