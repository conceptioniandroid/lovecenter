package com.marvix.lovetv.model;

public class BlogModel {
    String int_glcode;
    String var_title;
    String var_image;
    String var_categories;
    String var_description;
    String date;
    String var_postedby;
    boolean Staus;

    public boolean isStaus() {
        return Staus;
    }

    public void setStaus(boolean staus) {
        Staus = staus;
    }

    public String getVar_postedby() {
        return var_postedby;
    }

    public void setVar_postedby(String var_postedby) {
        this.var_postedby = var_postedby;
    }

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getVar_title() {
        return var_title;
    }

    public void setVar_title(String var_title) {
        this.var_title = var_title;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getVar_categories() {
        return var_categories;
    }

    public void setVar_categories(String var_categories) {
        this.var_categories = var_categories;
    }

    public String getVar_description() {
        return var_description;
    }

    public void setVar_description(String var_description) {
        this.var_description = var_description;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
