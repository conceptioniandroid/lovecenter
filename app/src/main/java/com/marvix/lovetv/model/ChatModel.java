package com.marvix.lovetv.model;

import java.util.ArrayList;

/**
 * Created by admin on 12/30/2017.
 */

public class ChatModel {
    String comment_id;
    String blog_id;
    String user_id;
    String var_name;
    String var_massage;
    String date;
    String time;
    String var_image;

    public ArrayList<ChatReplyModel> getChatReplyModelArrayList() {
        return chatReplyModelArrayList;
    }

    public void setChatReplyModelArrayList(ArrayList<ChatReplyModel> chatReplyModelArrayList) {
        this.chatReplyModelArrayList = chatReplyModelArrayList;
    }

    ArrayList<ChatReplyModel> chatReplyModelArrayList = new ArrayList<>();

    public ChatModel() {
    }

    public ChatModel(ArrayList<ChatReplyModel> chatReplyModelArrayList){
        this.chatReplyModelArrayList = chatReplyModelArrayList;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getComment_id() {
        return comment_id;
    }

    public void setComment_id(String comment_id) {
        this.comment_id = comment_id;
    }

    public String getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(String blog_id) {
        this.blog_id = blog_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVar_name() {
        return var_name;
    }

    public void setVar_name(String var_name) {
        this.var_name = var_name;
    }

    public String getVar_massage() {
        return var_massage;
    }

    public void setVar_massage(String var_massage) {
        this.var_massage = var_massage;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }



//    String type, name, img, time, message,id;
//
//    public String getId() {
//        return id;
//    }
//
//    public void setId(String id) {
//        this.id = id;
//    }
//
//    public ArrayList<ChatReplyModel> getChatReplyModelArrayList() {
//        return chatReplyModelArrayList;
//    }
//
//    public void setChatReplyModelArrayList(ArrayList<ChatReplyModel> chatReplyModelArrayList) {
//        this.chatReplyModelArrayList = chatReplyModelArrayList;
//    }
//
//    ArrayList<ChatReplyModel> chatReplyModelArrayList = new ArrayList<>();
//
    public ChatModel(ArrayList<ChatReplyModel> chatReplyModelArrayList,
                     String comment_id,
                     String blog_id,
                     String user_id,
                     String var_name,
                     String var_massage,
                     String date,
                     String time) {
        this.comment_id = comment_id;
        this.blog_id = blog_id;
        this.user_id = user_id;
        this.var_name = var_name;
        this.time = time;
        this.var_massage = var_massage;
        this.date = date;
        this.chatReplyModelArrayList = chatReplyModelArrayList;

    }
//
//    public ChatModel(ArrayList<ChatReplyModel> chatReplyModelArrayList){
//        this.chatReplyModelArrayList = chatReplyModelArrayList;
//    }
//
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getImg() {
//        return img;
//    }
//
//    public void setImg(String img) {
//        this.img = img;
//    }
//
//    public String getTime() {
//        return time;
//    }
//
//    public void setTime(String time) {
//        this.time = time;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
}
