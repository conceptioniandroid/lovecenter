package com.marvix.lovetv.model;

/**
 * Created by admin on 12/30/2017.
 */

public class ChatReplyModel {

    String reply_id;
    String blog_id;
    String user_id;
    String var_name;
    String var_massage;
    String  new_name;
    String new_message;
    String date;
    String time;
    String var_image;

    public ChatReplyModel() {
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getReply_id() {
        return reply_id;
    }

    public void setReply_id(String reply_id) {
        this.reply_id = reply_id;
    }

    public String getBlog_id() {
        return blog_id;
    }

    public void setBlog_id(String blog_id) {
        this.blog_id = blog_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVar_name() {
        return var_name;
    }

    public void setVar_name(String var_name) {
        this.var_name = var_name;
    }

    public String getVar_massage() {
        return var_massage;
    }

    public void setVar_massage(String var_massage) {
        this.var_massage = var_massage;
    }

    public String getNew_name() {
        return new_name;
    }

    public void setNew_name(String new_name) {
        this.new_name = new_name;
    }

    public String getNew_message() {
        return new_message;
    }

    public void setNew_message(String new_message) {
        this.new_message = new_message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    //    String type, name, img, time, message;
//    int id;
//
//    public int getId() {
//        return id;
//    }
//
//    public void setId(int id) {
//        this.id = id;
//    }
//
    public ChatReplyModel(String reply_id,
                          String blog_id,
                          String user_id,
                          String var_name,
                          String var_massage,
                          String  new_name,
                          String new_message,
                          String date,
                          String time) {

            this.reply_id=reply_id;
            this.blog_id=blog_id;
            this.user_id=user_id;
            this.var_name=var_name;
            this.var_massage=var_massage;
            this.new_name=new_name;
            this.new_message=new_message;
            this.date=date;
            this.time=time;
    }
//
//    public String getType() {
//        return type;
//    }
//
//    public void setType(String type) {
//        this.type = type;
//    }
//
//    public String getName() {
//        return name;
//    }
//
//    public void setName(String name) {
//        this.name = name;
//    }
//
//    public String getImg() {
//        return img;
//    }
//
//    public void setImg(String img) {
//        this.img = img;
//    }
//
//    public String getTime() {
//        return time;
//    }
//
//    public void setTime(String time) {
//        this.time = time;
//    }
//
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//    }
}
