package com.marvix.lovetv.model;

public class EventModel {
    String int_glcode;
    String var_title;
    String var_image;
    String date;
    String time;
    String end_date;
    String address;

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getVar_title() {
        return var_title;
    }

    public void setVar_title(String var_title) {
        this.var_title = var_title;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
