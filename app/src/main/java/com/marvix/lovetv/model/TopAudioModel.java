package com.marvix.lovetv.model;

public class TopAudioModel {
    String int_glcode;
    String var_image;
    String var_link;
    String var_title;
    String var_description;
    String audio_len;
    String audio_lenCopy;
    String Audio_Length;

    public String getAudio_Length() {
        return Audio_Length;
    }

    public void setAudio_Length(String audio_Length) {
        Audio_Length = audio_Length;
    }

    public String getAudio_len() {
        return audio_len;
    }

    public void setAudio_len(String audio_len) {
        this.audio_len = audio_len;
    }

    public String getAudio_lenCopy() {
        return audio_lenCopy;
    }

    public void setAudio_lenCopy(String audio_lenCopy) {
        this.audio_lenCopy = audio_lenCopy;
    }

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getVar_link() {
        return var_link;
    }

    public void setVar_link(String var_link) {
        this.var_link = var_link;
    }

    public String getVar_title() {
        return var_title;
    }

    public void setVar_title(String var_title) {
        this.var_title = var_title;
    }

    public String getVar_description() {
        return var_description;
    }

    public void setVar_description(String var_description) {
        this.var_description = var_description;
    }
}
