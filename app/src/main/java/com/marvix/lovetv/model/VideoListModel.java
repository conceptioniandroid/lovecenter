package com.marvix.lovetv.model;

public class VideoListModel {
    String int_glcode;
    String var_image;
    String var_link;
    String var_title;
    String var_description;

    public String getInt_glcode() {
        return int_glcode;
    }

    public void setInt_glcode(String int_glcode) {
        this.int_glcode = int_glcode;
    }

    public String getVar_image() {
        return var_image;
    }

    public void setVar_image(String var_image) {
        this.var_image = var_image;
    }

    public String getVar_link() {
        return var_link;
    }

    public void setVar_link(String var_link) {
        this.var_link = var_link;
    }

    public String getVar_title() {
        return var_title;
    }

    public void setVar_title(String var_title) {
        this.var_title = var_title;
    }

    public String getVar_description() {
        return var_description;
    }

    public void setVar_description(String var_description) {
        this.var_description = var_description;
    }
}
