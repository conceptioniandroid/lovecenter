package com.marvix.lovetv.service;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

public class AudioService extends Service {

    private AudioServiceBinder audioServiceBinder = new AudioServiceBinder();

    public AudioService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return audioServiceBinder;
    }
}
