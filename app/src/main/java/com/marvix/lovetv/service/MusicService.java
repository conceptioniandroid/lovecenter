package com.marvix.lovetv.service;

import android.annotation.SuppressLint;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

public class MusicService extends IntentService implements MediaPlayer.OnCompletionListener {

    MediaPlayer mediaPlayer;
    private static final String AUDIO_URL = "";

    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     *
     * @param name Used to name the worker thread, important only for debugging.
     */
    public MusicService(String name) {
        super(name);
    }

    public MusicService() {
        super("PlayAudio");

    }

    public static Intent getAudio(Context context, String audiourl) {
        return new Intent(context, MusicService.class)
                .putExtra(AUDIO_URL, audiourl);
    }

    public void onstop() {
        Log.d("TAG", "onstop: ");
        if (mediaPlayer.isPlaying()){
            Log.d("TAG", "onstop: ");
            mediaPlayer.pause();
        }
    }


    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String audio = intent.getStringExtra(AUDIO_URL);
        PlayAudio(audio);
    }

    private void PlayAudio(String audiourl) {
        Log.d("TAG", "PlayAudio: " + audiourl);
        mediaPlayer = new MediaPlayer();
        Uri audio;
        audio = Uri.parse(audiourl);
        if (mediaPlayer != null) {
            mediaPlayer = mediaPlayer.create(MusicService.this, audio);
            mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

            mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @SuppressLint("NewApi")
                @Override
                public void onPrepared(MediaPlayer mediaPlayer) {
                    if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                        mediaPlayer.stop();
                    } else {
                        if (mediaPlayer != null) {
                            Log.d("TAG", "PlayAudio:start ");
                            mediaPlayer.start();
                        }
                    }
                }
            });
        }
    }

    public static void OnStopListener(){

    }

    @Override
    public void onCompletion(MediaPlayer mp) {
        mediaPlayer.stop();
        mediaPlayer.reset();
        mediaPlayer.release();
        stopSelf();
    }

    public void onDestroy() {
        Log.d("TAG", "onDestroy:start ");
        if (mediaPlayer.isPlaying()) {
            mediaPlayer.stop();
        }
        mediaPlayer.reset();
        mediaPlayer.release();
    }
}
