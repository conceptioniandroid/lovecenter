package com.marvix.lovetv.utils;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;

import static com.marvix.lovetv.activity.HomeActivity.btnPlay;
import static com.marvix.lovetv.activity.HomeActivity.notificationBuilder;
import static com.marvix.lovetv.activity.HomeActivity.notificationManager;

public class CloseMusicReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("CloseMusicReceiver", "onReceive: ");

        if (HomeActivity.mp != null) {
            if (Constant.isPlaying) {
                HomeActivity.mp.pause();
                Constant.isPlaying = false;
                if (Constant.isScreen)
                    btnPlay.setImageResource(R.drawable.play);
            }
        }

        NotificationManager notificationManager = (NotificationManager) context.getApplicationContext().getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancel(0);

    }

}
