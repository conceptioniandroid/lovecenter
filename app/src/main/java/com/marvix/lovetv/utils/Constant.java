package com.marvix.lovetv.utils;

import android.util.DisplayMetrics;

public class Constant {

    /*api base url*/
    public static String ApiUrl = "https://felixosborn.org/api/";
    public static String registration = ApiUrl + "registration.php";
    public static String login = ApiUrl + "login.php";
    public static String cherech = ApiUrl + "cherech.php";
    public static String videoList = ApiUrl + "video_list.php";
    public static String audioList = ApiUrl + "audio_list.php";
    public static String blog = ApiUrl + "blog_list.php";
    public static String videoDetail = ApiUrl + "user_video_details.php";
    public static String event = ApiUrl + "event_list.php";
    public static String blogShare = ApiUrl + "blog_list.php";
    public static String addComment = ApiUrl + "insert_comment.php";
    public static String commentList = ApiUrl + "blog_comment_list.php";
    public static String Forgotpassword = ApiUrl + "forgotpassword.php";
    public static String ChangePassword = ApiUrl + "ChangePassword.php";
    public static String notAvailable = "N/A";

    public static String flag = "";
    public static String whichAdapter = "";

    public static String FlagFromPlay = "";

    public static boolean isDownload = false;
    public static boolean isPlaying = true;
    public static boolean isScreen = false;

}
