package com.marvix.lovetv.utils;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.marvix.lovetv.model.EventModel;
import com.marvix.lovetv.model.TopAudioModel;

import java.util.ArrayList;

public class DBOpenHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Lovetv.db";
    private static final int DATABASE_VERSION = 3;

    String TABLE_EVENTLIST = "EVENTLIST";
    String TABLE_BLOGLIST = "BLOGLIST";
    String TABLE_AUDIOLIST = "AUDIOLIST";
    Context context;


    public DBOpenHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "Create table " + TABLE_EVENTLIST + " (int_glcode INTEGER,data TEXT)";
        db.execSQL(query);

        String query1 = "Create table " + TABLE_BLOGLIST + " (int_glcode INTEGER,data TEXT)";
        db.execSQL(query1);

        String query2 = "Create table " + TABLE_AUDIOLIST + " (int_glcode INTEGER,var_image TEXT,var_link TEXT,var_title TEXT,var_description TEXT,audio_len TEXT,Audio_Length TEXT)";
        db.execSQL(query2);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_BLOGLIST);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_AUDIOLIST);
        onCreate(db);
    }

    public String getEventList() {

        String data = "";
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from " + TABLE_EVENTLIST, null);
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                data = res.getString(res.getColumnIndex("data"));
                res.moveToNext();
            }
        }
        return data;
    }

    public String getBlogList() {

        String data = "";
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from " + TABLE_BLOGLIST, null);
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                data = res.getString(res.getColumnIndex("data"));
                res.moveToNext();
            }
        }
        return data;
    }

    public ArrayList<TopAudioModel> getAudioList() {

        ArrayList<TopAudioModel> topAudioModelArrayList = new ArrayList<>();

        String data = "";
        SQLiteDatabase db = this.getReadableDatabase();
        @SuppressLint("Recycle") Cursor res = db.rawQuery("select * from " + TABLE_AUDIOLIST, null);
        if (res.getCount() > 0) {
            res.moveToFirst();
            for (int i = 0; i < res.getCount(); i++) {
                String int_glcode = res.getString(res.getColumnIndex("int_glcode"));
                String var_image = res.getString(res.getColumnIndex("var_image"));
                String var_link = res.getString(res.getColumnIndex("var_link"));
                String var_title = res.getString(res.getColumnIndex("var_title"));
                String var_description = res.getString(res.getColumnIndex("var_description"));
                String audio_len = res.getString(res.getColumnIndex("audio_len"));
                String Audio_Length = res.getString(res.getColumnIndex("Audio_Length"));

                TopAudioModel topAudioModel = new TopAudioModel();
                topAudioModel.setInt_glcode(int_glcode);
                topAudioModel.setVar_image(var_image);
                topAudioModel.setVar_link(var_link);
                topAudioModel.setVar_title(var_title);
                topAudioModel.setVar_description(var_description);
                topAudioModel.setAudio_len(audio_len);
                topAudioModel.setAudio_Length(Audio_Length);

                topAudioModelArrayList.add(topAudioModel);

                res.moveToNext();
            }
        }
        return topAudioModelArrayList;
    }

    public long addEvent(String data) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("int_glcode", "1");
        cv.put("data", data);
        return db.insert(TABLE_EVENTLIST, null, cv);
    }

    public long addBlog(String data) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("int_glcode", "1");
        cv.put("data", data);
        return db.insert(TABLE_BLOGLIST, null, cv);
    }

    public long addAudio(String int_glcode, String var_image, String var_link, String var_title, String var_description, String audio_len, String Audio_Length) {
        SQLiteDatabase db = getWritableDatabase();
        ContentValues cv = new ContentValues();
        cv.put("int_glcode", int_glcode);
        cv.put("var_image", var_image);
        cv.put("var_link", var_link);
        cv.put("var_title", var_title);
        cv.put("var_description", var_description);
        cv.put("audio_len", audio_len);
        cv.put("Audio_Length", Audio_Length);
        return db.insert(TABLE_AUDIOLIST, null, cv);
    }

    public void deleteEventTable() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_EVENTLIST, null, null);
    }

    public void deleteBlogTable() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_BLOGLIST, null, null);
    }

    public void deleteAudio() {
        SQLiteDatabase database = this.getWritableDatabase();
        database.delete(TABLE_AUDIOLIST, null, null);
    }


}
