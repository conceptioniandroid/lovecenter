package com.marvix.lovetv.utils;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.widget.Toast;

import com.marvix.lovetv.R;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Retrofit;

public class DownloadService extends IntentService {

    String url = "";
    String path = "";
    String name = "";
    String int_glcode = "";
    String image = "";
    String var_link = "";
    String var_title = "";
    String var_description = "";
    String audio_len = "";
    String Audio_Length = "";
    String TAG = "DownloadServiceData";
    DBOpenHelper dbOpenHelper;
    SQLiteDatabase sqLiteDatabase;

    public DownloadService() {
        super("Download Service");
    }

    private NotificationCompat.Builder notificationBuilder;
    private NotificationManager notificationManager;
    private int totalFileSize;

    @Override
    protected void onHandleIntent(Intent intent) {

        path = String.valueOf(getApplicationInfo().dataDir);

        int_glcode = intent.getStringExtra("int_glcode");
        image = intent.getStringExtra("image");
        var_link = intent.getStringExtra("var_link");
        var_title = intent.getStringExtra("var_title");
        var_description = intent.getStringExtra("var_description");
        audio_len = intent.getStringExtra("audio_len");
        Audio_Length = intent.getStringExtra("Audio_Length");
        name = System.currentTimeMillis() + "lovetv";

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        String CHANNEL_ID = "my_channel_01";
        CharSequence name = getString(R.string.app_name);// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        Uri uri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                    NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
                    mChannel.setName("LoveTv");
                    notificationManager.createNotificationChannel(mChannel);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        notificationBuilder = new NotificationCompat.Builder(this,CHANNEL_ID)
                .setSmallIcon(R.drawable.lv_tv)
                .setLargeIcon(BitmapFactory.decodeResource(getResources(), R.drawable.lv_tv))
                .setContentTitle("Download")
                .setContentText("Downloading File")
                .setAutoCancel(true);
        notificationManager.notify(1, notificationBuilder.build());


        initDownload();

    }

    private void initDownload() {

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://download.learn2crack.com/")
                .build();

        RetrofitInterface retrofitInterface = retrofit.create(RetrofitInterface.class);
        Call<ResponseBody> request = retrofitInterface.downloadFile(var_link);
        try {
            downloadFile(request.execute().body());
        } catch (IOException e) {
            e.printStackTrace();
            Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void downloadFile(ResponseBody body) throws IOException {

        int count;
        byte data[] = new byte[1024 * 4];
        long fileSize = body.contentLength();
        InputStream bis = new BufferedInputStream(body.byteStream(), 1024 * 8);
        File file = new File(path);
        if (!file.exists())
            file.mkdir();
        File outputFile = new File(path, name);
        OutputStream output = new FileOutputStream(outputFile);
        long total = 0;
        long startTime = System.currentTimeMillis();
        int timeCount = 1;
        while ((count = bis.read(data)) != -1) {

            total += count;
            totalFileSize = (int) (fileSize / (Math.pow(1024, 2)));
            double current = Math.round(total / (Math.pow(1024, 2)));

            int progress = (int) ((total * 100) / fileSize);

            long currentTime = System.currentTimeMillis() - startTime;

            Download download = new Download();
            download.setTotalFileSize(totalFileSize);

            if (currentTime > 1000 * timeCount) {

                download.setCurrentFileSize((int) current);
                download.setProgress(progress);
                sendNotification(download);
                timeCount++;
            }

            output.write(data, 0, count);
        }
        onDownloadComplete();
        output.flush();
        output.close();
        bis.close();

    }

    private void sendNotification(Download download) {

        sendIntent(download);
        notificationBuilder.setProgress(100, download.getProgress(), false);
        notificationBuilder.setContentText("Downloading file " + download.getCurrentFileSize() + "/" + totalFileSize + " MB");
        notificationManager.notify(1, notificationBuilder.build());
    }

    private void sendIntent(Download download) {

        Intent intent = new Intent("message_progress");
        intent.putExtra("download", download);
        LocalBroadcastManager.getInstance(DownloadService.this).sendBroadcast(intent);
    }

    private void onDownloadComplete() {

        File outputFile = new File(path, name);
        if (outputFile.exists())
            Log.d(TAG, "onDownloadComplete: " + outputFile.toString());
        else
            Log.d(TAG, "Notexist: " + outputFile.toString());

        Download download = new Download();
        download.setProgress(100);
        sendIntent(download);

        notificationManager.cancel(1);
        notificationBuilder.setProgress(0, 0, false);
        notificationBuilder.setContentText("File Downloaded");
        notificationManager.notify(1, notificationBuilder.build());

        dbOpenHelper = new DBOpenHelper(this);
        // dbOpenHelper.deleteAudio();
        sqLiteDatabase = dbOpenHelper.getWritableDatabase();
        long d = dbOpenHelper.addAudio(int_glcode, image, name, var_title, var_description, audio_len, Audio_Length);
        Log.d(TAG, "onDownloadComplete: " + d);

        Constant.isDownload = false;

    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        notificationManager.cancel(0);
    }

}
