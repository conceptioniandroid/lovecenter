package com.marvix.lovetv.utils;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

public class EditTextExtended extends EditText {

    public EditTextExtended(Context context) {
        super(context);
        setFontStyle(context,null);
    }

    public EditTextExtended(Context context, AttributeSet attrs) {
        super(context, attrs);
        setFontStyle(context,attrs);
    }

    public EditTextExtended(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setFontStyle(context,attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public EditTextExtended(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setFontStyle(context,attrs);
    }

    @Override
    public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
        InputConnection conn = super.onCreateInputConnection(outAttrs);
        outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
        return conn;
    }

    public void setFontStyle(Context context, AttributeSet attrs){
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "fonts/QUICKSAND-REGULAR_0.TTF");
        setTypeface(tf);
    }
}
