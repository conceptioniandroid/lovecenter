package com.marvix.lovetv.utils;

import android.view.Gravity;
import android.widget.Toast;


import com.marvix.lovetv.LoveCenter;

public class MakeToast {

    public MakeToast(String messageToDisplay){
        Toast toast = Toast.makeText(LoveCenter.getContext(),messageToDisplay, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
    public MakeToast(int stringResource){
        Toast toast = Toast.makeText(LoveCenter.getContext(),stringResource, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
