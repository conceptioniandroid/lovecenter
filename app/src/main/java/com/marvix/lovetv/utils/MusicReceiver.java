package com.marvix.lovetv.utils;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

import com.marvix.lovetv.R;
import com.marvix.lovetv.activity.HomeActivity;

import static com.marvix.lovetv.activity.HomeActivity.btnPlay;
import static com.marvix.lovetv.activity.HomeActivity.notificationBuilder;
import static com.marvix.lovetv.activity.HomeActivity.notificationManager;

public class MusicReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MusicReceiver", "onReceive: ");

        if (HomeActivity.mp != null) {
            RemoteViews notificationView = new RemoteViews(context.getPackageName(),
                    R.layout.notification_layout);

            if (Constant.isPlaying) {
                HomeActivity.mp.pause();
                Constant.isPlaying = false;
                notificationView.setImageViewResource(R.id.ivplay, R.drawable.play);
                if (Constant.isScreen)
                    btnPlay.setImageResource(R.drawable.play);
            } else {
                HomeActivity.mp.start();
                Constant.isPlaying = true;
                notificationView.setImageViewResource(R.id.ivplay, R.drawable.puse);
                if (Constant.isScreen)
                    btnPlay.setImageResource(R.drawable.puse);
            }

            notificationBuilder.setContent(notificationView);
            notificationManager.notify(0, notificationBuilder.build());
        }

    }

}
