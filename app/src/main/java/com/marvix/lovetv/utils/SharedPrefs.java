package com.marvix.lovetv.utils;

import android.content.Context;
import android.content.SharedPreferences;


import com.marvix.lovetv.LoveCenter;

public class SharedPrefs {

    public static SharedPreferences getSharedPref() {
        String sharedPrefenceName = "LoveCenter";
        return LoveCenter.getContext().getSharedPreferences(sharedPrefenceName, Context.MODE_PRIVATE);
    }

    public interface userSharedPrefData {
       String Phone_No = "Phone_No";
       String user_id = "user_id";
       String var_name = "var_name";
       String registration_id = "registration_id";
       String percentage = "percentage";
    }

    public interface tokendetail {
        String refreshtoken = "refreshtoken";
    }

}
