package com.marvix.lovetv.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextviewLight extends TextView {

    public TextviewLight(Context context) {
        super(context);
        init();
    }

    public TextviewLight(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextviewLight(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/QUICKSAND-LIGHT_0.TTF");
        setTypeface(tf);
    }
}
