package com.marvix.lovetv.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TextviewMedium extends TextView {

    public TextviewMedium(Context context) {
        super(context);
        init();
    }

    public TextviewMedium(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public TextviewMedium(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(),
                "fonts/QUICKSAND-MEDIUM_0.TTF");
        setTypeface(tf);
    }
}
